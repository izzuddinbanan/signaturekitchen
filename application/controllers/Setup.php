<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		user_validate();
		
		$this->load->model("setup_m");
    }
	
	public function index()
	{
		redirect("setup/user");
	}
	
	public function user($encrypted_data = ""){
		if((isset(role_setting('1')->view_only) && role_setting('1')->view_only == "1") || $this->session->userdata("username") == "admin"){
			if($this->input->post()){
				if($this->input->post('sort-column')){
					$search_array = unserialize( base64url_decode($encrypted_data));
					$search_array = array_merge($search_array,$this->input->post());

					$encrypted_data = base64url_encode(serialize($search_array));

					redirect('setup/user/' .$encrypted_data . '/' . $this->input->post('sort-page'));
				}
				$encrypted_data = base64url_encode(serialize($this->input->post()));
				redirect('setup/user/' .$encrypted_data);
			}
			
			$data = $this->setup_m->user_listing($encrypted_data);
			$data['title'] = "Signature Kitchen | User";
			$data['pagination'] = $this->pagination->create_links();
			$data['search_data'] = unserialize(base64url_decode($encrypted_data));
			
			$this->load->view("setup/user_v", $data);
		}
		else{
			redirect("dashboard");
		}
	}
	
	public function user_form($id=""){
		$data['title'] = "Signature Kitchen | User";
		
		if($id > 0){
			$data['user_detail'] = $this->setup_m->user_detail($id);
		}
		
		if($this->input->post()){
			if($id > 0){
				if($this->setup_m->update_user($id, $this->input->post())){
					redirect("setup/user_form/".$id);
				}
				else{
					$data['post_data'] = (object)$this->input->post();
				}
			}
			else{
				if($this->setup_m->add_user($this->input->post())){
					redirect("setup/user_form");
				}
				else{
					$data['post_data'] = (object)$this->input->post();
				}
			}
		}

		$data['active_role'] = $this->setup_m->active_role();
		$data['active_team'] = $this->setup_m->active_team();
		$this->load->view("setup/user_form_v", $data);
	}
	
	public function team($encrypted_data=""){
		if(isset(role_setting('2')->view_only) && role_setting('2')->view_only == "1" || $this->session->userdata("username") == "admin"){
			if($this->input->post()){
				if($this->input->post('sort-column')){
					$search_array = unserialize( base64url_decode($encrypted_data));
					$search_array = array_merge($search_array,$this->input->post());

					$encrypted_data = base64url_encode(serialize($search_array));

					redirect('setup/user/' .$encrypted_data . '/' . $this->input->post('sort-page'));
				}
				$encrypted_data = base64url_encode(serialize($this->input->post()));
				redirect('setup/user/' .$encrypted_data);
			}
			
			$data = $this->setup_m->team_listing($encrypted_data);
			$data['title'] = "Signature Kitchen | Team";
			$data['pagination'] = $this->pagination->create_links();
			$data['search_data'] = unserialize(base64url_decode($encrypted_data));
			$this->load->view("setup/team_v", $data);
			
		}
		else{
			redirect("dashboard");
		}
	}
	
	public function team_form($id=""){
		$data['title'] = "Signature Kitchen | Team";
		
		if($id > 0){
			$data['team_detail'] = $this->setup_m->team_detail($id);
		}
		
		if($this->input->post()){
			if($id > 0){
				if($this->setup_m->update_team($id, $this->input->post())){
					redirect("setup/team_form/".$id);
				}
				else{
					$data['post_data'] = (object)$this->input->post();
				}
			}
			else{
				if($this->setup_m->add_team($this->input->post())){
					redirect("setup/team_form");
				}
				else{
					$data['post_data'] = (object)$this->input->post();
				}
			}
		}
		
		$data['active_user'] = $this->setup_m->active_user();
		$this->load->view("setup/team_form_v", $data);
	}
	
	public function role($encrypted_data=''){
		if(isset(role_setting('3')->view_only) && role_setting('3')->view_only == "1" || $this->session->userdata("username") == "admin"){
			if($this->input->post()){
				if($this->input->post('sort-column')){
					$search_array = unserialize( base64url_decode($encrypted_data));
					$search_array = array_merge($search_array,$this->input->post());

					$encrypted_data = base64url_encode(serialize($search_array));

					redirect('setup/role/' .$encrypted_data . '/' . $this->input->post('sort-page'));
				}
				$encrypted_data = base64url_encode(serialize($this->input->post()));
				redirect('setup/role/' .$encrypted_data);
			}
			
			$data = $this->setup_m->role_listing($encrypted_data);
			$data['pagination'] = $this->pagination->create_links();
			$data['search_data'] = unserialize(base64url_decode($encrypted_data));
			$data['title'] = "Signature Kitchen | Role";
			
			$this->load->view("setup/role_v", $data);
		}
		else{
			redirect("dashboard");
		}
	}
	
	public function role_form($id=""){
		$data['title'] = "Signature Kitchen | Role";
		
		if($id > 0){
			$data['get_role_setting'] = $this->setup_m->get_role_setting($id);
		}
		
		if($this->input->post()){
			if($id > 0){
				if($this->setup_m->update_role($id, $this->input->post())){
					redirect("setup/role_form/" .$id);
				}
				else{
					$data['post_data'] = (object)$this->input->post();
				}
			}
			else{
				if($this->setup_m->add_role($this->input->post())){
					redirect("setup/role_form");
				}
				else{
					$data['post_data'] = (object)$this->input->post();
				}
			}
		}
		
		$data['get_page_access'] = $this->setup_m->get_page_access();
		$this->load->view("setup/role_form_v", $data);
	}
	
	public function subcont($encrypted_data=''){
		if(isset(role_setting('11')->view_only) && role_setting('11')->view_only == "1" || $this->session->userdata("username") == "admin"){
			if($this->input->post()){
				if($this->input->post('sort-column')){
					$search_array = unserialize( base64url_decode($encrypted_data));
					$search_array = array_merge($search_array,$this->input->post());

					$encrypted_data = base64url_encode(serialize($search_array));

					redirect('setup/subcont/' .$encrypted_data . '/' . $this->input->post('sort-page'));
				}
				$encrypted_data = base64url_encode(serialize($this->input->post()));
				redirect('setup/subcont/' .$encrypted_data);
			}
			
			$data = $this->setup_m->subc_listing($encrypted_data);
			$data['pagination'] = $this->pagination->create_links();
			$data['search_data'] = unserialize(base64url_decode($encrypted_data));
			$data['title'] = "Signature Kitchen | Subcont";
			
			$this->load->view("setup/subcont_v", $data);
		}
		else{
			redirect("dashboard");
		}
	}
	
	public function subcont_form($id=""){
		$data['title'] = "Signature Kitchen | Role";
		
		if($id > 0){
			$data['subcont_details'] = $this->setup_m->get_subcont_detailed($id); 
		}
		
		if($this->input->post()){
			if($id > 0){
				if($this->setup_m->update_subcont($id, $this->input->post())){
					redirect("setup/subcont_form");
				}
				else{
					$data['post_data'] = (object)$this->input->post();
				}
			}
			else{
				if($this->setup_m->add_subcont($this->input->post())){
					redirect("setup/subcont_form");
				}
				else{
					$data['post_data'] = (object)$this->input->post();
				}
			}
		}
		
		$this->load->view("setup/subcont_form_v", $data);
	}
	
	public function project_setup($encrypted_data=''){
		if(isset(role_setting('4')->view_only) && role_setting('4')->view_only == "1" || $this->session->userdata("username") == "admin"){
			if($this->input->post()){
				if($this->input->post('sort-column')){
					$search_array = unserialize( base64url_decode($encrypted_data));
					$search_array = array_merge($search_array,$this->input->post());

					$encrypted_data = base64url_encode(serialize($search_array));

					redirect('setup/project_setup/' .$encrypted_data . '/' . $this->input->post('sort-page'));
				}
				$encrypted_data = base64url_encode(serialize($this->input->post()));
				redirect('setup/project_setup/' .$encrypted_data);
			}
			
			$data = $this->setup_m->project_listing($encrypted_data);
			$data['pagination'] = $this->pagination->create_links();
			$data['search_data'] = unserialize(base64url_decode($encrypted_data));
			
			$data['title'] = "Signature Kitchen | Project Setup";
			$this->load->view("setup/project_setup_v", $data);
		}
		else{
			redirect("dashboard");
		}
	}
	
	public function project_form($id=''){
		$data['title'] = "Signature Kitchen | Project Setup";
		
		if($id > 0){
			$data['detail'] = $this->setup_m->get_detail_project($id);
		}
		
		if($this->input->post()){
			if($id > 0){
				if($this->setup_m->update_project($id, $this->input->post())){
					redirect("setup/project_form/".$id);
				}
				else{
					$data['post_data'] = (object)$this->input->post();
				}
			}
			else{
				if($this->setup_m->add_project($this->input->post())){
					redirect("setup/project_form");
				}
				else{
					$data['post_data'] = (object)$this->input->post();
				}
			}
		}
		
		$data['active_user'] = $this->setup_m->active_user(); 
		$this->load->view("setup/project_form_v", $data);
	}
	
	public function get_project_team(){
		if($this->input->is_ajax_request()){
			$id = $this->input->post('id');
			$member =  array();
			
			$team = $this->setup_m->get_project_team($id);
			
			if(is_array($team) && sizeof($team) > 0){
				foreach($team as $team_member){
					$member[] = $team_member['username'];
				}
			}
			
			$data['user'] = implode(', ', $member);
			echo json_encode($data);
		}
	}
	
	public function global_setting(){
		if(isset(role_setting('5')->view_only) && role_setting('5')->view_only == "1" || $this->session->userdata("username") == "admin"){
			$data['title'] = "Signature Kitchen | Global Setting";
			$this->load->view("setup/global_setting_v", $data);
		}
		else{
			redirect("dashboard");
		}
	}
}
