<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		user_validate();
    }
	
	public function index()
	{
		redirect("payment/new_subcont_bq");
	}
	
	public function new_subcont_bq(){
		$data['title'] = "Signature Kitchen | New Subcont BQ";
		$this->load->view("progress_payment/new_subcont_bq_v", $data);
	}
	
	public function list_subcont_bq(){
		$data['title'] = "Signature Kitchen | List Subcont BQs";
		$this->load->view("progress_payment/list_subcont_bq_v", $data);
	}
	
	public function new_ipc(){
		$data['title'] = "Signature Kitchen | New IPC";
		$this->load->view("progress_payment/new_ipc_v",$data);
	}
	
	public function list_ipc(){
		$data['title'] = "Signature Kitchen | List IPC";
		$this->load->view("progress_payment/list_ipc_v", $data);
	}
}
