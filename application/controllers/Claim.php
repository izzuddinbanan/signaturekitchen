<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Claim extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		user_validate();
    }
	
	public function index()
	{
		redirect("claim/new_bq");
	}
	
	public function list_bq(){
		if(isset(role_setting('7')->view_only) && role_setting('7')->view_only == "1" || $this->session->userdata("username") == "admin"){
			$data['title'] = "Signature Kitchen | List BQ";
			$this->load->view("progress_claim/list_bq_v", $data);
			
		}
		else{
			redirect('dashboard');
		}
		
	}
	
	public function new_bq(){
		if(isset(role_setting('7')->view_only) && role_setting('7')->view_only == "1" || $this->session->userdata("username") == "admin"){
			
			if($this->input->post()){
				ad($this->input->post()); exit;
			}
			
			$data['title'] = "Signature Kitchen | New BQ";
			$this->load->view("progress_claim/new_bq_v", $data);
			
		}
		else{
			redirect('dashboard');
		}
		
	}
	
	public function bq_setup(){
		if(isset(role_setting('7')->view_only) && role_setting('7')->view_only == "1" || $this->session->userdata("username") == "admin"){
			
			if($this->input->post()){
				ad($this->input->post()); exit;
			}
			
			$data['title'] = "Signature Kitchen | BQ Setup";
			$this->load->view("progress_claim/bq_setup_v", $data);
			
		}
		else{
			redirect('dashboard');
		}
		
	}
	
	public function bq_vo(){
		if(isset(role_setting('7')->view_only) && role_setting('7')->view_only == "1" || $this->session->userdata("username") == "admin"){
			$data['title'] = "Signature Kitchen | BQ VO";
			$this->load->view("progress_claim/bq_vo_v", $data);
			
		}
		else{
			redirect('dashboard');
		}
		
	}
	
	public function list_progress_claim(){
		if(isset(role_setting('8')->view_only) && role_setting('8')->view_only == "1" || $this->session->userdata("username") == "admin"){
			$data['title'] = "Signature Kitchen | List Progress Claim";
			$this->load->view("progress_claim/list_progress_claim_v", $data);
			
		}
		else{
			redirect('dashboard');
		}
		
	}
	
	public function new_progress_claim(){
		if(isset(role_setting('8')->view_only) && role_setting('8')->view_only == "1" || $this->session->userdata("username") == "admin"){
			$data['title'] = "Signature Kitchen | New Progress Claim";
			$this->load->view("progress_claim/new_progress_claim_v", $data);
			
		}
		else{
			redirect('dashboard');
		}
		
	}
	
}
