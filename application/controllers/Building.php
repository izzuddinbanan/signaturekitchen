<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Building extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		user_validate();
		
		$this->load->model("building_m");
    }
	
	public function index()
	{
		redirect("building/list_graph");
	}
	
	public function graph_form($project_id="", $completion_id="", $building_id=""){
		if(isset(role_setting('6')->view_only) && role_setting('6')->view_only == "1" || $this->session->userdata("username") == "admin"){
			
			if($this->input->post()){
				if($building_id > 0){
					if($this->building_m->update_graph($project_id, $completion_id, $building_id, $this->input->post())){
						redirect("building/graph_form/".$project_id."/".$completion_id);
					}
				}
				else{
					if($this->building_m->add_graph($project_id, $completion_id, $this->input->post())){
						redirect("building/graph_form/".$project_id."/".$completion_id);
					}
				}
			}
			
			$data['title'] = "Signature Kitchen | New Building Graph";
			$data['project_setup'] = $this->building_m->get_project_setup($project_id, $completion_id);
			$this->load->view("building_graph/new_building_v", $data);
			
		}
		else{
			redirect("dashboard");
		}
	}
	
	public function list_graph($encrypted_data=''){
		if(isset(role_setting('6')->view_only) && role_setting('6')->view_only == "1" || $this->session->userdata("username") == "admin"){
			if($this->input->post()){
				if($this->input->post('sort-column')){
					$search_array = unserialize( base64url_decode($encrypted_data));
					$search_array = array_merge($search_array,$this->input->post());

					$encrypted_data = base64url_encode(serialize($search_array));

					redirect('building/list_graph/' .$encrypted_data . '/' . $this->input->post('sort-page'));
				}
				$encrypted_data = base64url_encode(serialize($this->input->post()));
				redirect('building/list_graph/' .$encrypted_data);
			}
			
			$data = $this->building_m->graph_listing($encrypted_data);
			$data['pagination'] = $this->pagination->create_links();
			$data['search_data'] = unserialize(base64url_decode($encrypted_data));
			$data['title'] = "Signature Kitchen | List Building Graph";
			$this->load->view("building_graph/list_v", $data);
		}
		else{
			redirect("dashboard");
		}
	}
}
