<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		$this->load->model("login_m");
		
		if($this->uri->segment(2) == 'signout'){
			user_validate();
		}
		else{
			user_invalidate();
		}
    }
	
	public function index()
	{
		$data = array();
		
		if($this->input->post()){
			if($this->login_m->login_verify($this->input->post())){
				redirect("dashboard/home");
			}
			else{
				$data['failed'] = 'Login Failed';
			}
		}
		
		$this->load->view("login/login_v", $data);
	}
	
	public function signout(){
		#unset session
		$this->session->unset_userdata(array(
					'id',
					'email',
					'logged_in',
					'last_logged',
			));
		
		redirect("login");
	}
}
