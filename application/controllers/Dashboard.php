<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		user_validate();
    }
	
	public function index()
	{
		redirect("dashboard/home");
	}
	
	public function home(){
		
		$data['title'] = "Signiture Kitchen | Home";
		$this->load->view("dashboard/index_v", $data);
	}
	
}
