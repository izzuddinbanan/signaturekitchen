<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

					<!-- BEGIN: Aside Menu -->
					<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
						<ul class="m-menu__nav ">
							<li class="m-menu__item <?php echo $this->uri->segment(1) == 'dashboard' ? "m-menu__item--active" : '' ?> " aria-haspopup="true"><a href="<?php echo base_url()?>dashboard" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon fa fa-chalkboard"></i><span class="m-menu__link-text">Dashboard</span></a></li>
							<?php 
							if((isset(role_setting('1')->view_only) && role_setting('1')->view_only == '1' ) || (isset(role_setting('2')->view_only) && role_setting('2')->view_only == '1') || (isset(role_setting('3')->view_only) && role_setting('3')->view_only == '1' ) || (isset(role_setting('11')->view_only) && role_setting('11')->view_only == '1' ) || (isset(role_setting('4')->view_only) && role_setting('4')->view_only == '1' ) || (isset(role_setting('5')->view_only) && role_setting('5')->view_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
							<li class="m-menu__item  m-menu__item--submenu <?php echo $this->uri->segment(1) == 'setup' ? 'm-menu__item--open m-menu__item--expanded' : ''?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i
									 class="m-menu__link-icon fa fa-cogs"></i><span class="m-menu__link-text">Master Setup</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<?php 
										if((isset(role_setting('1')->view_only) && role_setting('1')->view_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'user' ? "m-menu__item--active" : ''?>" aria-haspopup="true"><a href="<?php echo base_url(); ?>setup/user" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">User</span></a></li>
										<?php
										}
										if((isset(role_setting('2')->view_only) && role_setting('2')->view_only == '1') || $this->session->userdata("username") == "admin"){ ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'team' ? "m-menu__item--active" : ''?>" aria-haspopup="true" ><a href="<?php echo base_url(); ?>setup/team" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Team</span></a></li>
										<?php
										}
										if((isset(role_setting('3')->view_only) && role_setting('3')->view_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'role' ? "m-menu__item--active" : ''?>" aria-haspopup="true" ><a href="<?php echo base_url(); ?>setup/role" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Role</span></a></li>
										<?php
										}
										if((isset(role_setting('11')->view_only) && role_setting('11')->view_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'subcont' ? "m-menu__item--active" : ''?>" aria-haspopup="true" ><a href="<?php echo base_url(); ?>setup/subcont" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Subcont (+Installer)</span></a></li>
										<?php
										}
										if((isset(role_setting('4')->view_only) && role_setting('4')->view_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'project_setup' ? "m-menu__item--active" : ''?>" aria-haspopup="true" ><a href="<?php echo base_url(); ?>setup/project_setup" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Project Setup</span></a></li>
										<?php
										}
										if((isset(role_setting('5')->view_only) && role_setting('5')->view_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'global_setting' ? "m-menu__item--active" : ''?>" aria-haspopup="true" ><a href="<?php echo base_url(); ?>setup/global_setting" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Global Setting</span></a></li>
										<?php	
										}
										?>
									</ul>
								</div>
							</li>
							<?php
							}
							if((isset(role_setting('6')->view_only) && role_setting('6')->view_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
							<li class="m-menu__item  m-menu__item--submenu <?php echo $this->uri->segment(1) == 'building' ? 'm-menu__item--open m-menu__item--expanded' : ''?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i
									 class="m-menu__link-icon fa fa-building"></i><span class="m-menu__link-text">Building Graph</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<?php 
										if((isset(role_setting('5')->add_only) && role_setting('5')->add_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
											<!--<li class="m-menu__item <?php echo $this->uri->segment(2) == 'graph_form' ? "m-menu__item--active" : ''?>" aria-haspopup="true"><a href="<?php echo base_url(); ?>building/graph_form" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">New Building Graph</span></a></li>-->
										<?php
										}
										?>
										<li class="m-menu__item <?php echo $this->uri->segment(2) == 'list_graph' ? "m-menu__item--active" : ''?>" aria-haspopup="true"><a href="<?php echo base_url(); ?>building/list_graph" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Building Graphs</span></a></li>
									</ul>
								</div>
							</li>
							<?php	
							}
							if((isset(role_setting('8')->view_only) && role_setting('8')->view_only == "1") || (isset(role_setting('7')->view_only) && role_setting('7')->view_only == "1") || $this->session->userdata("username") == "admin"){ ?>
							<li class="m-menu__item  m-menu__item--submenu <?php echo $this->uri->segment(1) == 'claim' ? 'm-menu__item--open m-menu__item--expanded' : ''?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i
									 class="m-menu__link-icon fa fa-columns"></i><span class="m-menu__link-text">Progress Claim</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<?php 
										if((isset(role_setting('7')->view_only) && role_setting('7')->view_only == '1' ) || $this->session->userdata("username") == "admin"){
											if((isset(role_setting('7')->add_only) && role_setting('7')->add_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'new_bq' ? "m-menu__item--active" : ''?>" aria-haspopup="true"><a href="<?php echo base_url(); ?>claim/new_bq" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">New BQ</span></a></li>
											<?php
											} ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'list_bq' ? "m-menu__item--active" : ''?>" aria-haspopup="true" ><a href="<?php echo base_url(); ?>claim/list_bq" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List BQs</span></a></li>
										<?php
										}
										if((isset(role_setting('8')->view_only) && role_setting('8')->view_only == '1' ) || $this->session->userdata("username") == "admin"){
											if((isset(role_setting('8')->add_only) && role_setting('8')->add_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'new_progress_claim' ? "m-menu__item--active" : ''?>" aria-haspopup="true" ><a href="<?php echo base_url(); ?>claim/new_progress_claim" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">New Progress Claim</span></a></li>
											<?php
											} ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'list_progress_claim' ? "m-menu__item--active" : ''?>" aria-haspopup="true" ><a href="<?php echo base_url(); ?>claim/list_progress_claim" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Progress Claims</span></a></li>
										<?php
										}
										?>
									</ul>
								</div>
							</li>
							<?php
							}
							if((isset(role_setting('9')->view_only) && role_setting('9')->view_only == "1") || (isset(role_setting('10')->view_only) && role_setting('10')->view_only == "1") || $this->session->userdata("username") == "admin"){ ?>
							<li class="m-menu__item  m-menu__item--submenu <?php echo $this->uri->segment(1) == 'payment' ? 'm-menu__item--open m-menu__item--expanded' : ''?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i
									 class="m-menu__link-icon fa fa-money-bill-wave"></i><span class="m-menu__link-text">Progress Payment</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<?php 
										if((isset(role_setting('9')->view_only) && role_setting('9')->view_only == '1' ) || $this->session->userdata("username") == "admin"){
											if((isset(role_setting('9')->add_only) && role_setting('9')->add_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'new_subcont_bq' ? "m-menu__item--active" : ''?>" aria-haspopup="true"><a href="<?php echo base_url(); ?>payment/new_subcont_bq" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">New Subcont BQ</span></a></li>
											<?php
											} ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'list_subcont_bq' ? "m-menu__item--active" : ''?>" aria-haspopup="true" ><a href="<?php echo base_url(); ?>payment/list_subcont_bq" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Subcont BQs</span></a></li>
										<?php
										}
										if((isset(role_setting('10')->view_only) && role_setting('10')->view_only == '1' ) || $this->session->userdata("username") == "admin"){
											if((isset(role_setting('10')->add_only) && role_setting('10')->add_only == '1' ) || $this->session->userdata("username") == "admin"){ ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'new_ipc' ? "m-menu__item--active" : ''?>" aria-haspopup="true" ><a href="<?php echo base_url(); ?>payment/new_ipc" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">New IPC</span></a></li>
											<?php
											} ?>
											<li class="m-menu__item <?php echo $this->uri->segment(2) == 'list_ipc' ? "m-menu__item--active" : ''?>" aria-haspopup="true" ><a href="<?php echo base_url(); ?>payment/list_ipc" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List IPC</span></a></li>
										<?php
										}
										?>
									</ul>
								</div>
							</li>
							<?php
							}
							?>
						</ul>
					</div>

					<!-- END: Aside Menu -->
				</div>

				<!-- END: Left Aside -->