<?php $this->load->view("login/signup_header_v")?>
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url(<?php echo base_url(); ?>assets/app/media/img//bg/bg-8.jpg);">
				<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
								<img src="<?php echo base_url(); ?>assets/app/media/img/logos/signature-logo.png">
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title" style="color: white;">Sign In To Admin</h3>
							</div>
							<form class="m-login__form m-form" action="<?php echo base_url();?>login" method="POST">
								<?php 
								if(isset($failed) && $failed != ''){
									echo '<div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn" role="alert">		
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>			
											<span>'.$failed.'</span>		
										</div>';
								}
								
								?>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" style="background: #ffffff9e; color: #000;">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" style="background: #ffffff9e; color: #000;">
								</div>
								<div class="row m-login__form-sub">
									<div class="col m--align-left m-login__form-left">
										<label class="m-checkbox  m-checkbox--light" style="color: white;">
											<input type="checkbox" name="remember" > Remember me
											<span></span>
										</label>
									</div>
									<div class="col m--align-right m-login__form-right">
										<a href="javascript:;" id="m_login_forget_password" class="m-link" style="color: white;">Forget Password ?</a>
									</div>
								</div>
								<div class="m-login__form-action">
									<button  class="btn m-btn--pill m-btn--air m-login__btn m-login__btn--secondary" style="background-color: #c4c5d;">Sign In</button>
								</div>
							</form>
						</div>
						<div class="m-login__signup">
							<div class="m-login__head">
								<h3 class="m-login__title" style="color: white;">Sign Up</h3>
								<div class="m-login__desc" style="color: white;">Enter your details to create your account:</div>
							</div>
							<form class="m-login__form m-form" action="">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Fullname" name="fullname" style="background: #ffffff9e; color: #000;">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" style="background: #ffffff9e; color: #000;">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="password" placeholder="Password" name="password" style="background: #ffffff9e; color: #000;">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirm Password" name="rpassword" style="background: #ffffff9e; color: #000;">
								</div>
								<div class="row form-group m-form__group m-login__form-sub">
									<div class="col m--align-left">
										<label class="m-checkbox m-checkbox--light" style="color: white;">
											<input type="checkbox" name="agree">I Agree the <a href="#" class="m-link m-link--focus" style="color: white;"> terms and conditions</a>.
											<span></span>
										</label>
										<span class="m-form__help"></span>
									</div>
								</div>
								<div class="m-login__form-action">
									<button id="m_login_signup_submit" class="btn m-btn--pill m-btn--air m-login__btn m-login__btn--secondary" style="background-color: #c4c5d;">Sign Up</button>&nbsp;&nbsp;
									<button id="m_login_signup_cancel" class="btn m-btn--pill btn-outline-metal  m-login__btn" style="border-color: #fff">Cancel</button>
								</div>
							</form>
						</div>
						<div class="m-login__forget-password">
							<div class="m-login__head">
								<h3 class="m-login__title" style="color: white;">Forgotten Password ?</h3>
								<div class="m-login__desc" style="color: white;">Enter your email to reset your password:</div>
							</div>
							<form class="m-login__form m-form" action="">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off" style="background: #ffffff9e; color: #000;">
								</div>
								<div class="m-login__form-action">
									<button id="m_login_forget_password_submit" class="btn m-btn--pill m-btn--air m-login__btn m-login__btn--secondary" style="background-color: #c4c5d;">Request</button>&nbsp;&nbsp;
									<button id="m_login_forget_password_cancel" class="btn m-btn--pill btn-outline-metal  m-login__btn" style="border-color: #fff">Cancel</button>
								</div>
							</form>
						</div>
						<div class="m-login__account">
							<span class="m-login__account-msg" style="color: white;">
								Don't have an account yet ?
							</span>&nbsp;&nbsp;
							<a href="javascript:;" id="m_login_signup" class="m-link m-link--light m-login__account-link" style="color: white;">Sign Up</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- end:: Page -->
<?php $this->load->view("login/signup_footer_v")?>
