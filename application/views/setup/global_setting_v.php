<?php $this->load->view("header_v")?>

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Master Setup</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-cogs"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Global Setting</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->

					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">

								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--full-height">
									<!-- <div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
													Default Accordions
												</h3>
											</div>
										</div>
									</div> -->
									<div class="m-portlet__body">

										<!--begin::Section-->
										<div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">

											<!--begin::Item-->
											<div class="m-accordion__item">
												<div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="    false">
													<span class="m-accordion__item-icon"><i class="fa flaticon-user-ok"></i></span>
													<span class="m-accordion__item-title">Scope of Work</span>
													<span class="m-accordion__item-mode"></span>
												</div>
												<div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
													<div class="m-accordion__item-content">
														<ul> 
															<?php 
															foreach(scope_work() as $scope_work){ ?>
																<li><?php echo $scope_work; ?></li>
															<?php 
															}
															?>
														</ul>
													</div>
												</div>
											</div>

											<!--end::Item-->

											<!--begin::Item-->
											<div class="m-accordion__item">
												<div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_2_head" data-toggle="collapse" href="#m_accordion_1_item_2_body" aria-expanded="    false">
													<span class="m-accordion__item-icon"><i class="fa flaticon-presentation"></i></span>
													<span class="m-accordion__item-title">Sub Contract Works</span>
													<span class="m-accordion__item-mode"></span>
												</div>
												<div class="m-accordion__item-body collapse" id="m_accordion_1_item_2_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_2_head" data-parent="#m_accordion_1">
													<div class="m-accordion__item-content">
														<ul> 
															<?php 
															foreach(subcont_work() as $subcont){ ?>
																<li><?php echo $subcont; ?></li>
															<?php 
															}
															?>
														</ul>
													</div>
												</div>
											</div>

											<!--end::Item-->

											<!--begin::Item-->
											<div class="m-accordion__item">
												<div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_3_head" data-toggle="collapse" href="#m_accordion_1_item_3_body" aria-expanded="    false">
													<span class="m-accordion__item-icon"><i class="fa  flaticon-presentation-1"></i></span>
													<span class="m-accordion__item-title">Contractor Services</span>
													<span class="m-accordion__item-mode"></span>
												</div>
												<div class="m-accordion__item-body collapse" id="m_accordion_1_item_3_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_3_head" data-parent="#m_accordion_1">
													<div class="m-accordion__item-content">
														<ul> 
															<?php 
															foreach(contractor_services() as $contractor){ ?>
																<li><?php echo $contractor; ?></li>
															<?php 
															}
															?>
														</ul>
													</div>
												</div>
											</div>

											<!--end::Item-->
										</div>

										<!--end::Section-->
									</div>
								</div>

								<!--end::Portlet-->
							</div>
						</div>
					</div>
						
						
<?php $this->load->view("footer_v")?>