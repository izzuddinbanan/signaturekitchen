<?php $this->load->view("header_v")?>
<?php 
	$id = $this->uri->segment("3");
	if($id > 0){
		$post_data = $user_detail->row();
	}
?>
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Master Setup</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-cogs"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">User</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->

					<div class="m-content">
						<!--begin::Portlet-->
						<div class="m-portlet m-portlet--tab">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" name="userform" method="POST" action="<?php echo base_url()?>setup/user_form/<?php echo $id;?>">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10">
										<h3 class="alert m-alert m-alert--default" role="alert">
											<?php echo $id > 0 ? (isset(role_setting('1')->edit_only) && role_setting('1')->edit_only == "1") || $this->session->userdata("username") == "admin" ? "Update user" : "User Profile" : "Add new user";?>
										</h3>
									</div>
									<div class="form-group m-form__group m--margin-top-10">
										<?php get_msg(); ?>
									</div>
									<div class="form-group m-form__group m-form__group--md row">
										<label for="example-text-input" class="col-2 col-form-label">Username <i style="color: red">*</i></label>
										<div class="col-10">
											<input class="form-control form-control-sm m-input" type="text" value="<?php echo isset($post_data->username) && $post_data->username != '' ? $post_data->username : '' ?>" name="username">
										</div>
									</div>
									<?php 
									if($id == 0){ ?>
										<div class="form-group m-form__group m-form__group--md row">
											<label for="example-password-input" class="col-2 col-form-label">Password <i style="color: red">*</i></label>
											<div class="col-10">
												<input class="form-control form-control-sm m-input" type="password" value="" name="password">
											</div>
										</div>
									<?php
									}
									?>
									
									<div class="form-group m-form__group m-form__group--md row">
										<label for="example-email-input" class="col-2 col-form-label">Email <i style="color: red">*</i></label>
										<div class="col-10">
											<input class="form-control form-control-sm m-input" type="email" value="<?php echo isset($post_data->email) && $post_data->email != '' ? $post_data->email : '' ?>" name="email">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Contact Number</label>
										<div class="col-10">
											<input class="form-control form-control-sm m-input" type="tel" value="<?php echo isset($post_data->contact) && $post_data->contact != '' ? $post_data->contact : '' ?>" name="contact">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Team</label>
										<div class="col-10">
											<select class="form-control form-control-sm m-input " name="team">
												<option></option>
												<?php 
												if($active_team->num_rows() > 0){
													foreach($active_team->result() as $row_team){ ?>
														<option value= "<?php echo isset($row_team->id) && $row_team->id != '' ? $row_team->id : ''?>" <?php echo isset($row_team->id) && isset($post_data->team) && $row_team->id == $post_data->team ? 'selected' : ''?>><?php echo isset($row_team->team_name) && $row_team->team_name != '' ? $row_team->team_name : '' ?></option>
												<?php
													}
												}
												?>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Role</label>
										<div class="col-10">
											<select class="form-control form-control-sm m-input " name="role">
												<option></option>
												<?php 
												if($active_role->num_rows() > 0){
													foreach($active_role->result() as $row_role){ ?>
														<option value= "<?php echo isset($row_role->id) && $row_role->id != '' ? $row_role->id : ''?>" <?php echo isset($row_role->id) && isset($post_data->role) && $row_role->id == $post_data->role ? 'selected' : '' ?>><?php echo isset($row_role->role_name) && $row_role->role_name != '' ? $row_role->role_name :''?></option>
												<?php
													}
												}
												?>
											</select>
										</div>	
									</div>
									<div class="m-form__group form-group m-form__group--md row">
										
										<label class="col-2 col-form-label">Active</label>
										<div class="col-10">
											<span class="m-switch m-switch--sm m-switch--icon m-switch--info">
												<label>
													<input type="checkbox"  name="active" value="1" <?php echo isset($post_data->active) && $post_data->active == '1' ? 'checked' : '' ?>>
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2">
											</div>
											<div class="col-10">
												<?php 
												if((isset(role_setting('1')->edit_only) && role_setting('1')->edit_only == "1") || $this->session->userdata("username") == "admin"){ ?>
													<button type="submit" class="btn btn-sm btn-success">Submit</button>
												<?php
												}
												?>
												<a href="<?php echo base_url() ?>setup/user" class="btn btn-sm btn-secondary">Back</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
<?php $this->load->view("footer_v")?>
<script>
$(function(){
	$(".dropdown").select2({
		placeholder: "Please Select"
	});
	
});
</script>