<?php $this->load->view("header_v")?>
<?php 
	$id = $this->uri->segment("3");
	if($id > 0){
		$post_data = $team_detail->row();
	}
?>
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Master Setup</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-cogs"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">team</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->

					<div class="m-content">
						<!--begin::Portlet-->
						<div class="m-portlet m-portlet--tab">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" name="userform" method="POST" action="<?php echo base_url()?>setup/team_form/<?php echo $id;?>">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10">
										<h3 class="alert m-alert m-alert--default" role="alert">
											<?php echo $id > 0 ? (isset(role_setting('2')->edit_only) && role_setting('2')->edit_only == "1") || $this->session->userdata("username") == "admin" ? "Update team" : "Team Detail" : "Add new team";?>
										</h3>
									</div>
									<div class="form-group m-form__group m--margin-top-10">
										<?php get_msg(); ?>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="team_name" class="col-2 col-form-label">Team <i style="color: red">*</i></label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->team_name) && $post_data->team_name != '' ? $post_data->team_name : '' ?>" name="team_name">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="team_leader" class="col-2 col-form-label">Team Leader</label>
										<div class="col-10">
											<select class="form-control form-control-sm  m-input" name="team_leader">
												<option></option>
												<?php 
												if($active_user->num_rows() > 0){
													foreach($active_user->result() as $row_user){ ?>
														<option value= "<?php echo $row_user->id ?>" <?php echo isset($row_user->id) && isset($post_data->team_leader) && $row_user->id == $post_data->team_leader ? 'selected' : ''?>><?php echo isset($row_user->username) && $row_user->username != "" ? $row_user->username : '' ?></option> <?php
													}
												}
												?>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-email-input" class="col-2 col-form-label">Team Email <i style="color: red">*</i></label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="email" value="<?php echo isset($post_data->team_email) && $post_data->team_email != '' ? $post_data->team_email : '' ?>" name="team_email">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="team_member" class="col-2 col-form-label">Team Member</label>
										<div class="col-10  div-append">
										<?php 
										$query_team_member = $this->setup_m->get_team_member("query", array('`a`.team_id' => $id));
										
										if($query_team_member->num_rows() > 0){
											foreach($query_team_member->result() as $row_member){ ?>
													<div class="row row_member" style="margin-bottom: 5px">
														<div class="col-11 ">
															<select class="form-control  form-control-sm m-input " name="team_member[]">
																<option></option>
																<?php 
																if($active_user->num_rows() > 0){
																	foreach($active_user->result() as $row_user){ 
																	?>
																		<option value= "<?php echo $row_user->id ?>" <?php echo isset($row_user->id) && isset($row_member->team_member) && $row_user->id == $row_member->team_member ? 'selected' : ''?>><?php echo isset($row_user->username) && $row_user->username != "" ? $row_user->username : '' ?></option> <?php
																	}
																}
																?>
															</select>
														</div>
														<div class="col-1">
															<button class="btn btn-sm btn-danger btn-delete-row" type="button"><span class="la la-close"></span></button>
														</div>
													</div>
												 <?php
											}
										}
										else{ ?>
											<div class="row row_member" style="margin-bottom: 5px">
												<div class="col-11 ">
													<select class="form-control form-control-sm m-input " name="team_member[]">
														<option></option>
														<?php 
														if($active_user->num_rows() > 0){
															foreach($active_user->result() as $row_user){ ?>
																<option value= "<?php echo $row_user->id ?>" <?php echo isset($row_user->id) && isset($post_data->team_leader) && $row_user->id == $post_data->team_leader ? 'selected' : ''?>><?php echo isset($row_user->username) && $row_user->username != "" ? $row_user->username : '' ?></option> <?php
															}
														}
														?>
													</select>
												</div>
												<div class="col-1 pb-1">
													<button class="btn btn-danger btn-sm btn-delete-row "  type="button"><span class="la la-close"></span></button>
												</div>
											</div>
										<?php
										}
										?>
										
										
										</div>
									</div>
									<div class="m-form__group form-group  m-form__group--md row">
										<div class="col-2">
										</div>
										<div class="col-10">
											<button type="button" class="btn btn-sm btn-info btn-add-row "><i class="la la-plus"></i> Add Team Member</button>
										</div>
									</div>
									<div class="m-form__group form-group  m-form__group--md row">
										<label class="col-2 col-form-label">Active</label>
										<div class="col-10">
											<input data-switch="true" type="checkbox" checked="checked" data-on-color="info">
											<span class="m-switch m-switch--sm m-switch--icon m-switch--info">
												<label>
													<input type="checkbox" name="active" value="1" <?php echo isset($post_data->active) && $post_data->active == '1' ? 'checked' : '' ?>>
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2">
											</div>
											<div class="col-10">
												<?php 
												if((isset(role_setting('2')->edit_only) && role_setting('2')->edit_only == "1") || $this->session->userdata("username") == "admin"){ ?>
												<button type="submit" class="btn btn-sm  btn-success">Submit</button>
												<?php
												}
												?>
												<a href="<?php echo base_url();?>setup/team" class="btn btn-sm btn-secondary">Back</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
						
<?php $this->load->view("footer_v")?>
<script>
$(function(){
	$(".dropdown").select2({
		placeholder: "Please Select",
		allowClear: true
	});
	
	$(".m-bootstrap-select").selectpicker();
	
	$('.btn-add-row').on('click', function(){
		var newRow = $('.row_member').first().clone();
		$('.div-append').append(newRow);
		
		// $(".dropdown").select2({
			// placeholder: "Please Select"
		// });
		
		// $(".dropdown").last().next().next().remove();
	});
	
	$('body').on('click', '.btn-delete-row', function(){
		if($('.row_member').length != 1){
			$(this).closest('.row_member').remove();
		}
	});
});
</script>