<?php $this->load->view("header_v")?>

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Master Setup</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-cogs"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">User</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->

					<div class="m-content">
						<div class="col-xl-12">
							<!--begin::Portlet-->
							<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text ">
												List of user
											</h3>
										</div>			
									</div>
									<?php 
									if((isset(role_setting('1')->add_only) && role_setting('1')->add_only == "1") || $this->session->userdata("username") == "admin"){ ?>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item">
													<a href="<?php echo base_url()?>setup/user_form" class="m-portlet__nav-link btn btn-sm btn-success m-btn m-btn--air">
														<i class="fa fa-plus-circle"></i>
														 Add New User
													</a>
												</li>
											</ul>
										</div>
									<?php
									}
									?>
								</div>
								<div class="m-portlet__body">
								
									<!--begin::Section-->
									<div class="m-section">
										<div class="row ">
											<div class="col-3" style="padding-bottom: 15px;" >
												<div class="m-input-icon m-input-icon--left">
													<form class="formsearch" method="post" action="" autocomplete="off">
														<input type="text" class="form-control m-input m-input--solid" placeholder="Search..." name="search">
													</form>
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span><i class="la la-search"></i></span>
													</span>
												</div>
											</div>
										</div>
										
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th>#</th>
															<th class="col-sort<?php echo isset($search_data['sort-column']) && $search_data['sort-column'] == "username" ? '-'. $search_data['sort-type'] : '' ?> " data-column="username">Username</th>
															<th class="col-sort<?php echo isset($search_data['sort-column']) && $search_data['sort-column'] == "email" ? '-'. $search_data['sort-type'] : '' ?> " data-column="email">Email</th>
															<th class="col-sort<?php echo isset($search_data['sort-column']) && $search_data['sort-column'] == "contact" ? '-'. $search_data['sort-type'] : '' ?> " data-column="contact">Contact Number</th>
															<th class="col-sort<?php echo isset($search_data['sort-column']) && $search_data['sort-column'] == "team" ? '-'. $search_data['sort-type'] : '' ?> " data-column="team">Team</th>
															<th class="col-sort<?php echo isset($search_data['sort-column']) && $search_data['sort-column'] == "role" ? '-'. $search_data['sort-type'] : '' ?> " data-column="role">Role</th>
															<th>Active</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
													<?php 
													$i = $starting_row;
													if($query_user->num_rows() > 0){
														foreach($query_user->result() as $row_user){ ?>
															<tr>
																<th scope="row" style="width: 1%;"><?php echo $i ?></th>
																<td><?php echo isset($row_user->username) && $row_user->username != "" ? ucwords($row_user->username) : '';?></td>
																<td><?php echo isset($row_user->email) && $row_user->email != "" ? $row_user->email : '';?></td>
																<td><?php echo isset($row_user->contact) && $row_user->contact != "" ? ucwords($row_user->contact) : '';?></td>
																<td><?php echo isset($row_user->team_name) && $row_user->team_name != "" ? ucwords($row_user->team_name) : '';?></td>
																<td><?php echo isset($row_user->role_name) && $row_user->role_name != "" ? ucwords($row_user->role_name) : '';?></td>
																<td style="width: 1%;"><?php echo isset($row_user->active) && $row_user->active == "1" ? '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Yes</span>' : '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">No</span>';?></td>
																<td style="width: 1%;">
																<a href="<?php echo base_url()?>setup/user_form/<?php echo $row_user->id?>" class="btn btn-secondary btn-sm m-btn m-btn--custom m-btn--label-accent" title="Edit">
																	<i class="la la-edit"></i>
																</a>
																</td>
															</tr>
													<?php
														$i++;
														}
													}
													else{ ?>
														<tr>
															<td colspan="8" style="text-align:center"> No Records Found</td>
														</tr>
													<?php
													}
													?>
												</tbody>
												</table>
											</div>
										</div>
										<div class="row">
											<div class="col">
												<?php echo $starting_row; ?> - <?php echo $stoping_row; ?> / <?php echo $total_row; ?>
											</div>
											<div class="col" style="margin-left: 80%;">
												<div><?php  echo $pagination; ?> </div>
											</div>
										</div>
									</div>
									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
						</div>
					</div>
						
<form action="" method="post" class="form-sort">
	<input type="hidden" name="sort-page" class="sort-page" value="<?php  echo $this->uri->segment(4); ?>" />
	<input type="hidden" name="sort-column" class="sort-column" value="" />
	<input type="hidden" name="sort-type" class="sort-type" value="" />
</form>
<?php $this->load->view("footer_v")?>
<script>
$(function(){
	initSorting();
	
	//sorting start
	$('.col-sort').on('click', function(){
		var varColumn = $(this).data('column');
		$('.form-sort').find('.sort-column').val(varColumn);
		$('.form-sort').find('.sort-type').val("asc");
		
		$('.form-sort').submit();
	});
	
	$('.col-sort-asc').on('click', function(){	
		var varColumn = $(this).data('column');
		$('.form-sort').find('.sort-column').val(varColumn);
		$('.form-sort').find('.sort-type').val("desc");
		
		$('.form-sort').submit();
	});
	
	$('.col-sort-desc').on('click', function(){
		var varColumn = $(this).data('column');
		$('.form-sort').find('.sort-column').val(varColumn);
		$('.form-sort').find('.sort-type').val("asc");
		
		$('.form-sort').submit();
	});
	//sorting end
	
});
</script>