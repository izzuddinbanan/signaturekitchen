<?php $this->load->view("header_v")?>

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Master Setup</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-cogs"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Team</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->

					<div class="m-content">
						<div class="col-xl-12">
							<!--begin::Portlet-->
							<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text ">
												List of team
											</h3>
										</div>			
									</div>
									<?php 
									if((isset(role_setting('2')->add_only) && role_setting('2')->add_only == "1") || $this->session->userdata("username") == "admin"){ ?>
									<div class="m-portlet__head-tools">
										<ul class="m-portlet__nav">
											<li class="m-portlet__nav-item">
												<a href="<?php echo base_url()?>setup/team_form" class="m-portlet__nav-link btn btn-sm btn-success m-btn m-btn--air">
													<i class="fa fa-plus-circle"></i>
													 Add New Team
												</a>
											</li>
										</ul>
									</div>
									<?php	
									}
									?>
								</div>
								<div class="m-portlet__body">
								
									<!--begin::Section-->
									<div class="m-section">
										<div class="row ">
											<div class="col-3" style="padding-bottom: 15px;" >
												<div class="m-input-icon m-input-icon--left">
													<form class="formsearch" method="post" action="" autocomplete="off">
														<input type="text" class="form-control m-input m-input--solid" placeholder="Search..." name="search">
													</form>
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span><i class="la la-search"></i></span>
													</span>
												</div>
											</div>
										</div>
										
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th>#</th>
															<th class="col-sort<?php echo isset($search_data['sort-column']) && $search_data['sort-column'] == "team_name" ? '-'. $search_data['sort-type'] : '' ?> " data-column="team_name">Team</th>
															<th class="col-sort<?php echo isset($search_data['sort-column']) && $search_data['sort-column'] == "team_leader" ? '-'. $search_data['sort-type'] : '' ?> " data-column="team_leader">Team Leader</th>
															<th class="col-sort<?php echo isset($search_data['sort-column']) && $search_data['sort-column'] == "team_email" ? '-'. $search_data['sort-type'] : '' ?> " data-column="team_email">Email</th>
															<th>Team Member</th>
															<th>Active</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
													<?php 
													$i = $starting_row;
													if($query_team->num_rows() > 0){
														foreach($query_team->result() as $row_team){ 
															$query_team_member = $this->setup_m->get_team_member("query", array('`a`.team_id' => $row_team->id));
															?>
															
															<tr>
																<th scope="row" style="width: 1%;"><?php echo $i ?></th>
																<td><?php echo isset($row_team->team_name) && $row_team->team_name != "" ? $row_team->team_name : '';?></td>
																<td><?php echo isset($row_team->username) && $row_team->username != "" ? $row_team->username : '';?></td>
																<td><?php echo isset($row_team->team_email) && $row_team->team_email != "" ? $row_team->team_email : '';?></td>
																<td><?php 
																	if($query_team_member->num_rows() > 0){
																		foreach($query_team_member->result() as $row_team_member){ ?>
																			<ul>
																				<li><?php echo isset($row_team_member->username) && $row_team_member->username != "" ? $row_team_member->username : '' ?></li>
																			</ul>
																		<?php
																		}
																	}
																?></td>
																<td style="width: 1%;"><?php echo isset($row_team->active) && $row_team->active == "1" ? '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Yes</span>' : '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">No</span>';?></td>
																<td style="width: 1%;">
																<a href="<?php echo base_url()?>setup/team_form/<?php echo $row_team->id?>" class="btn btn-secondary btn-sm m-btn m-btn--custom m-btn--label-accent" title="Edit">
																	<i class="la la-edit"></i>
																</a>
																</td>
															</tr>
													<?php
														$i++;
														}
													}
													else{ ?>
														<tr>
															<td colspan="8" style="text-align:center"> No Records Found</td>
														</tr>
													<?php
													}
													?>
												</tbody>
												</table>
											</div>
										</div>
										<div class="row">
											<div class="col">
												<?php echo $starting_row; ?> - <?php echo $stoping_row; ?> / <?php echo $total_row; ?>
											</div>
											<div class="col" style="margin-left: 80%;">
												<div><?php  echo $pagination; ?> </div>
											</div>
										</div>
									</div>
									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
						</div>
					</div>
						
						
<?php $this->load->view("footer_v")?>
<script>
$(function(){
	initSorting();
	
	//sorting start
	$('.col-sort').on('click', function(){
		var varColumn = $(this).data('column');
		$('.form-sort').find('.sort-column').val(varColumn);
		$('.form-sort').find('.sort-type').val("asc");
		
		$('.form-sort').submit();
	});
	
	$('.col-sort-asc').on('click', function(){	
		var varColumn = $(this).data('column');
		$('.form-sort').find('.sort-column').val(varColumn);
		$('.form-sort').find('.sort-type').val("desc");
		
		$('.form-sort').submit();
	});
	
	$('.col-sort-desc').on('click', function(){
		var varColumn = $(this).data('column');
		$('.form-sort').find('.sort-column').val(varColumn);
		$('.form-sort').find('.sort-type').val("asc");
		
		$('.form-sort').submit();
	});
	//sorting end
	
});
</script>