<?php $this->load->view("header_v")?>

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Master Setup</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-cogs"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Project Setup</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

						<!-- END: Subheader -->

					<div class="m-content">
						<div class="col-xl-12">
							<!--begin::Portlet-->
							<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text ">
												List of project
											</h3>
										</div>			
									</div>
									<div class="m-portlet__head-tools">
										<ul class="m-portlet__nav">
											<li class="m-portlet__nav-item">
												<a href="<?php echo base_url()?>setup/project_form" class="m-portlet__nav-link btn btn-sm btn-success m-btn m-btn--air">
													<i class="fa fa-plus-circle"></i>
													 Add New Project
												</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="m-portlet__body">
								
									<!--begin::Section-->
									<div class="m-section">
										<div class="row ">
											<div class="col-3" style="padding-bottom: 15px;" >
												<div class="m-input-icon m-input-icon--left">
													<form class="formsearch" method="post" action="" autocomplete="off">
														<input type="text" class="form-control m-input m-input--solid" placeholder="Search..." name="search">
													</form>
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span><i class="la la-search"></i></span>
													</span>
												</div>
											</div>
										</div>
										
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th>#</th>
															<th>Project Name</th>
															<th>Project Code</th>
															<th>Developer Name</th>
															<th>Project Company Name</th>
															<th>Maincon Company Name</th>
															<th>Architect Name</th>
															<th>Designer Name</th>
															<th>Sales Team </th>
															<th>Total Project Unit</th>
															<th>LOA Received Date</th>
															<th>Project DLP Date </th>
															<th>Project LAD Amount / Day</th>
															<th>Work in Progress To Date </th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<?php 
														$i = $starting_row;
														if($query_project->num_rows() > 0){
															foreach($query_project->result() as $row_project){ ?>
																<tr>
																	<td><?php echo $i ?></td>
																	<td><?php echo isset($row_project->project_name) && $row_project->project_name != "" ? $row_project->project_name  : '' ?></td>
																	<td><?php echo isset($row_project->project_code) && $row_project->project_code != "" ? $row_project->project_code  : '' ?></td>
																	<td><?php echo isset($row_project->dev_name) && $row_project->dev_name != "" ? $row_project->dev_name  : '' ?></td>
																	<td><?php echo isset($row_project->pro_company_name) && $row_project->pro_company_name != "" ? $row_project->pro_company_name  : '' ?></td>
																	<td><?php echo isset($row_project->main_company_name) && $row_project->main_company_name != "" ? $row_project->main_company_name  : '' ?></td>
																	<td><?php echo isset($row_project->architect_name) && $row_project->architect_name != "" ? $row_project->architect_name  : '' ?></td>
																	<td><?php echo isset($row_project->designer_name) && $row_project->designer_name != "" ? $row_project->designer_name  : '' ?></td>
																	<td><?php echo isset($row_project->sales_team) && $row_project->sales_team != "" ? $row_project->sales_team  : '' ?></td>
																	<td><?php echo isset($row_project->project_unit) && $row_project->project_unit != "" ? $row_project->project_unit  : '' ?></td>
																	<td><?php echo isset($row_project->loa_date) && $row_project->loa_date != "" ? date('Y-m-d', strtotime($row_project->loa_date)) : '' ?></td>
																	<td><?php echo isset($row_project->dlp_date) && $row_project->dlp_date != "" ? date('Y-m-d', strtotime($row_project->dlp_date)) : '' ?></td>
																	<td><?php echo isset($row_project->lad_amount) && $row_project->lad_amount != "" ? $row_project->lad_amount  : '' ?></td>
																	<td><?php echo isset($row_project->work_inprogress) && $row_project->work_inprogress != "" ? $row_project->work_inprogress  : '' ?></td>
																	<td>
																	<a href="<?php echo base_url()?>setup/project_form/<?php echo $row_project->id ?>" class="btn btn-secondary btn-sm m-btn m-btn--custom m-btn--label-accent" title="Edit">
																		<i class="la la-edit"></i>
																	</a>
																	</td>
																</tr>
														<?php
																$i++;
															}
														}
														else{ ?>
															<tr>
																<td colspan="15" style="text-align:center"> No Records Found</td>
															</tr>
														<?php
														}
														?>
												</tbody>
												</table>
											</div>
										</div>
										<div class="row">
											<div class="col">
												<?php echo $starting_row; ?> - <?php echo $stoping_row; ?> / <?php echo $total_row; ?>
											</div>
											<div class="col" style="margin-left: 80%;">
												<div><?php echo $pagination; ?> </div>
											</div>
										</div>
									</div>
									<!--end::Section-->
								</div>

								<!--end::Form-->
							</div>

							<!--end::Portlet-->
						</div>
					</div>
						
<form action="" method="post" class="form-sort">
	<input type="hidden" name="sort-page" class="sort-page" value="<?php  echo $this->uri->segment(4); ?>" />
	<input type="hidden" name="sort-column" class="sort-column" value="" />
	<input type="hidden" name="sort-type" class="sort-type" value="" />
</form>
<?php $this->load->view("footer_v")?>
<script>
$(function(){
	initSorting();
	
	//sorting start
	$('.col-sort').on('click', function(){
		var varColumn = $(this).data('column');
		$('.form-sort').find('.sort-column').val(varColumn);
		$('.form-sort').find('.sort-type').val("asc");
		
		$('.form-sort').submit();
	});
	
	$('.col-sort-asc').on('click', function(){	
		var varColumn = $(this).data('column');
		$('.form-sort').find('.sort-column').val(varColumn);
		$('.form-sort').find('.sort-type').val("desc");
		
		$('.form-sort').submit();
	});
	
	$('.col-sort-desc').on('click', function(){
		var varColumn = $(this).data('column');
		$('.form-sort').find('.sort-column').val(varColumn);
		$('.form-sort').find('.sort-type').val("asc");
		
		$('.form-sort').submit();
	});
	//sorting end
	
});
</script>