<?php $this->load->view("header_v")?>
<?php 
	$id = $this->uri->segment("3");
	if($id > 0){
		$post_data = $get_role_setting->row();
	}
?>
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Master Setup</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-cogs"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Role</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->

					<div class="m-content">
						<!--begin::Portlet-->
						<div class="m-portlet m-portlet--tab">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" name="userform" method="POST" action="<?php echo base_url()?>setup/role_form/<?php echo $id;?>">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10">
										<h3 class="alert m-alert m-alert--default" role="alert">
											<?php echo $id > 0 ? (isset(role_setting('3')->edit_only) && role_setting('3')->edit_only == "1") || $this->session->userdata("username") == "admin" ? "Update role" : "Role Setting" : "Add new role";?>
										</h3>
									</div>
									<div class="form-group m-form__group m--margin-top-10">
										<?php get_msg(); ?>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="team_name" class="col-2 col-form-label">Role <i style="color: red;">*</i></label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->role_name) && $post_data->role_name != '' ? $post_data->role_name : '' ?>" name="role_name">
										</div>
									</div>
									<?php 
									if($get_page_access->num_rows() > 0){
										foreach($get_page_access->result() as $row_page_access){ 
										
										if($id > 0){
											$privilege  = $this->setup_m->get_privilege('row', array('role_id' => $id, 'page_access_id' => $row_page_access->id));
										}
										?>
											<div class="form-group m-form__group  m-form__group--md row">
												<label for="" class="col-2 col-form-label"><?php echo isset($row_page_access->page_name) && $row_page_access->page_name != '' ? trim($row_page_access->page_name) : '' ?></label>
												<div class="col-10 m-checkbox-inline">
													&emsp;
													<label class="m-checkbox">
														<input type="checkbox" value="1"  name="view[<?php echo isset($row_page_access->id) && $row_page_access->id != '' ? trim($row_page_access->id) : '' ?>]" <?php echo isset($privilege->view_only) && $privilege->view_only == '1' ? 'checked' : ''?>> Can View
														<span></span>
													</label>
													<label class="m-checkbox">
														<input type="checkbox" value="1" name="add[<?php echo isset($row_page_access->id) && $row_page_access->id != '' ? trim($row_page_access->id) : '' ?>]" <?php echo isset($privilege->add_only) && $privilege->add_only == '1' ? 'checked' : '' ?>> Can Add
														<span></span>
													</label>
													<label class="m-checkbox">
														<input type="checkbox" value="1" name="edit[<?php echo isset($row_page_access->id) && $row_page_access->id != '' ? trim($row_page_access->id) : '' ?>]" <?php echo isset($privilege->edit_only) && $privilege->edit_only == '1' ? 'checked' : '' ?>> Can Edit
														<span></span>
													</label>
													<label class="m-checkbox">
														<input type="checkbox" value="1" name="delete[<?php echo isset($row_page_access->id) && $row_page_access->id != '' ? trim($row_page_access->id) : '' ?>]" <?php echo isset($privilege->delete_only) && $privilege->delete_only == '1' ? 'checked' : '' ?>> Can Delete
														<span></span>
													</label>
												</div>
											</div>
									<?php		
										}	
									}
									?>
									<div class="m-form__group form-group  m-form__group--md row">
										<label class="col-2 col-form-label">Active</label>
										<div class="col-10">
											<input data-switch="true" type="checkbox" checked="checked" data-on-color="info">
											<span class="m-switch m-switch--sm m-switch--icon m-switch--info">
												<label>
													<input type="checkbox" name="active" value="1" <?php echo isset($post_data->active) && $post_data->active == '1' ? 'checked' : '' ?>>
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2">
											</div>
											<div class="col-10">
												<?php 
												if((isset(role_setting('3')->edit_only) && role_setting('3')->edit_only == "1") || $this->session->userdata("username") == "admin"){ ?>
												<button type="submit" class="btn btn-sm btn-success">Submit</button>
												<?php
												}
												?>
											
												<a href="<?php echo base_url(); ?>setup/role" class="btn btn-sm btn-secondary">Back</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
						
<?php $this->load->view("footer_v")?>
<script>
$(function(){
	// $(".dropdown2").select2();
	
	$('.btn-add-row').on('click', function(){
		var newRow = $($('.row_member')[0]).clone();

		$('.div-append').append(newRow);
	});
	
	$('body').on('click', '.btn-delete-row', function(){
		if($('.row_member').length != 1){
			$(this).closest('.row_member').remove();
		}
	});
});
</script>