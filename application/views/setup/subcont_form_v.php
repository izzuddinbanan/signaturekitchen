<?php $this->load->view("header_v")?>
<?php 
	$id = $this->uri->segment("3");
	if($id > 0){
		$post_data = $subcont_details->row();
	}
?>
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Master Setup</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-cogs"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Subcont (+Installer)</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->

					<div class="m-content">
						<!--begin::Portlet-->
						<div class="m-portlet m-portlet--tab">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" name="userform" method="POST" action="<?php echo base_url()?>setup/subcont_form/<?php echo $id;?>">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10">
										<h3 class="alert m-alert m-alert--default" role="alert">
											<?php echo $id > 0 ? "Update subcont" : "Add new subcont";?>
										</h3>
									</div>
									<div class="form-group m-form__group m--margin-top-10">
										<?php get_msg(); ?>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-text-input" class="col-2 col-form-label">Subcont Code <i style="color: red">*</i></label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->sub_code) && $post_data->sub_code != '' ? $post_data->sub_code : '' ?>" name="sub_code">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-text-input" class="col-2 col-form-label">Subcont Name <i style="color: red">*</i></label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->sub_name) && $post_data->sub_name != '' ? $post_data->sub_name : '' ?>" name="sub_name">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Subcont Type</label>
										<div class="col-10">
											<select class="form-control  form-control-sm m-input " name="sub_type" id="sub_type">
												<option></option>
												<?php 
												foreach(contractor_services() as $subcont_type){ ?>
													<option value= "<?php echo $subcont_type?>" <?php echo isset($post_data->sub_type) && $post_data->sub_type == $subcont_type ? 'selected' : '' ?>><?php echo $subcont_type?></option>
												<?php
												}
												?>
											</select>
										</div>	
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-text-input" class="col-2 col-form-label">Company No <i style="color: red">*</i></label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->company_no) && $post_data->company_no != '' ? $post_data->company_no : '' ?>" name="company_no">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-text-input" class="col-2 col-form-label">GST Registration No <i style="color: red">*</i></label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->gst_reg_no) && $post_data->gst_reg_no != '' ? $post_data->gst_reg_no : '' ?>" name="gst_reg_no">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-text-input" class="col-2 col-form-label">Address <i style="color: red">*</i></label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->address) && $post_data->address != '' ? $post_data->address : '' ?>" name="address">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-email-input" class="col-2 col-form-label">PIC <i style="color: red">*</i></label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->pic) && $post_data->pic != '' ? $post_data->pic : '' ?>" name="pic">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Phone Number</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="tel" value="<?php echo isset($post_data->phone) && $post_data->phone != '' ? $post_data->phone : '' ?>" name="phone">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Email</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="email" value="<?php echo isset($post_data->email) && $post_data->email != '' ? $post_data->email : '' ?>" name="email">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">% of Certified Value Retained</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->cert_value) && $post_data->cert_value != '' ? $post_data->cert_value : '' ?>" name="cert_value">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Limit of Retention Fund</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->limit_retention) && $post_data->limit_retention != '' ? $post_data->limit_retention : '' ?>" name="limit_retention">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Payment Terms</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->payment_term) && $post_data->payment_term != '' ? $post_data->payment_term : '' ?>" name="payment_term">
										</div>
									</div>
									<div class="m-form__group form-group  m-form__group--md row">
										
										<label class="col-2 col-form-label">Active</label>
										<div class="col-10">
											<span class="m-switch m-switch--sm m-switch--icon m-switch--info">
												<label>
													<input type="checkbox"  name="active" value="1" <?php echo isset($post_data->active) && $post_data->active == '1' ? 'checked' : '' ?>>
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2">
											</div>
											<div class="col-10">
												<?php 
												if((isset(role_setting('11')->edit_only) && role_setting('11')->edit_only == "1") || $this->session->userdata("username") == "admin"){ ?>
												<button type="submit" class="btn btn-sm btn-success">Submit</button>
												<?php
												}
												?>
												<button type="button" class="btn btn-info" id="installer">Setup Installer Team</button>
												<a href="<?php echo base_url(); ?>setup/subcont" class="btn btn-sm btn-secondary">Back</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
						
						
<?php $this->load->view("footer_v")?>
<script>
function installer_setup(){
	var installer = '<?php echo isset($post_data->sub_type) && $post_data->sub_type != '' ? $post_data->sub_type : '' ?>';
	
	if(installer == 'Installer'){
		$('#installer').show();
	}
	else{
		$('#installer').hide();
	}
}

$(function(){
	installer_setup();
	
	$(".dropdown").select2({
		placeholder: "Please Select"
	});
	
	$('#sub_type').on('change', function(){
		if($(this).val() == 'Installer'){
			$('#installer').show();
		}
		else{
			$('#installer').hide();
		}
	});
});
</script>