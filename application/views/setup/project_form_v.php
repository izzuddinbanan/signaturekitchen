<?php 
	$this->load->view("header_v");

	$id = $this->uri->segment(3);
	if($id > 0){
		$post_data = $detail['project_setup']->row();
	}

?>
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Master Setup</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-cogs"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Project Setup</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->

					<div class="m-content">
						<!--begin::Portlet-->
						<div class="m-portlet m-portlet--tab">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" name="userform" method="POST" action="<?php echo base_url() ?>setup/project_form/<?php echo $id ?>">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10">
										<h3 class="alert m-alert m-alert--default" role="alert">
											Project Setup
										</h3>
									</div>
									<div class="form-group m-form__group m--margin-top-10">
										<?php get_msg(); ?>
									</div>
									<div class="form-group m-form__group m-form__group--md row">
										<label for="" class="col-2 col-form-label">Project Name</label>
										<div class="col-10">
											<select class="form-control form-control-sm m-input " name="project_name">
												<option></option>
												<option value="Apartment Qiqi" <?php echo isset($post_data->project_name) && $post_data->project_name == 'Apartment Qiqi' ? 'selected' : '' ?>>Apartment Qiqi</option>
												<option value="Alif Condominium" <?php echo isset($post_data->project_name) && $post_data->project_name == 'Alif Condominium' ? 'selected' : '' ?>>Alif Condominium</option>
												<option value="Apartment Baru" <?php echo isset($post_data->project_name) && $post_data->project_name == 'Apartment Baru' ? 'selected' : '' ?>>Apartment Baru</option>
												<option value="Bangunan Al Azhar" <?php echo isset($post_data->project_name) && $post_data->project_name == 'Bangunan Al Azhar' ? 'selected' : '' ?>>Bangunan Al Azhar</option>
											</select>
										</div>	
									</div>
									<div class="form-group m-form__group m-form__group--md row">
										<label for="" class="col-2 col-form-label">Project Code </label>
										<div class="col-10">
											<input class="form-control form-control-sm m-input" type="text" value="<?php echo isset($post_data->project_code) && $post_data->project_code != '' ? $post_data->project_code : '' ?>" name="project_code">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md row" >
										<label for="example-text-input" class="col-2 col-form-label">Developer Name </label>
										<div class="col-4">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->dev_name) && $post_data->dev_name != '' ? $post_data->dev_name : '' ?>" name="dev_name">
										</div>
										
										<label for="example-text-input" class="col-2 col-form-label">Contact </label>
										<div class="col-4">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->dev_contact) && $post_data->dev_contact != '' ? $post_data->dev_contact : '' ?>" name="dev_contact">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-text-input" class="col-2 col-form-label">Project Company Name</label>
										<div class="col-4">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->pro_company_name) && $post_data->pro_company_name != '' ? $post_data->pro_company_name : '' ?>" name="pro_company_name">
										</div>
										
										<label for="example-text-input" class="col-2 col-form-label">Contact </label>
										<div class="col-4">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->pro_company_contact) && $post_data->pro_company_contact != '' ? $post_data->pro_company_contact : '' ?>" name="pro_company_contact">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md  row">
										<label for="example-text-input" class="col-2 col-form-label">Maincon Company Name</label>
										<div class="col-4">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->main_company_name) && $post_data->main_company_name != '' ? $post_data->main_company_name : '' ?>" name="main_company_name">
										</div>
										
										<label for="example-text-input" class="col-2 col-form-label">Contact </label>
										<div class="col-4">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->main_company_contact) && $post_data->main_company_contact != '' ? $post_data->main_company_contact : '' ?>" name="main_company_contact">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md  row">
										<label for="example-text-input" class="col-2 col-form-label">Architect Name</label>
										<div class="col-4">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->architect_name) && $post_data->architect_name != '' ? $post_data->architect_name : '' ?>" name="architect_name">
										</div>
										
										<label for="example-text-input" class="col-2 col-form-label">Contact </label>
										<div class="col-4">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->architect_contact) && $post_data->architect_contact != '' ? $post_data->architect_contact : '' ?>" name="architect_contact">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-email-input" class="col-2 col-form-label">Designer Name </label>
										<div class="col-4">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->designer_name) && $post_data->designer_name != '' ? $post_data->designer_name : '' ?>" name="designer_name">
										</div>
										
										<label for="example-text-input" class="col-2 col-form-label">Contact </label>
										<div class="col-4">
											<input class="form-control form-control-sm m-input" type="text" value="<?php echo isset($post_data->designer_contact) && $post_data->designer_contact != '' ? $post_data->designer_contact : '' ?>" name="designer_contact">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Sales Team</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->sales_team) && $post_data->sales_team != '' ? $post_data->sales_team : '' ?>" name="sales_team">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Total Project Unit</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->project_unit) && $post_data->project_unit != '' ? $post_data->project_unit : '' ?>" name="project_unit">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">LOA Received Date</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input m_datepicker" type="text" value="<?php echo isset($post_data->loa_date) && $post_data->loa_date != '' ? date('Y-m-d', strtotime($post_data->loa_date)) : '' ?>" name="loa_date" autocomplete="off">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Project DLP Date</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input m_datepicker" type="text" value="<?php echo isset($post_data->dlp_date) && $post_data->dlp_date != '' ? date('Y-m-d', strtotime($post_data->dlp_date)) : '' ?>" name="dlp_date" autocomplete="off">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Project LAD Amount / Day</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->lad_amount) && $post_data->lad_amount != '' ? $post_data->lad_amount : '' ?>" name="lad_amount">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Target Completion Date </label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input m_datepicker" type="text" value="<?php echo isset($post_data->target_completion_date) && $post_data->target_completion_date != '' ? date('Y-m-d', strtotime($post_data->target_completion_date)) : '' ?>" name="target_completion_date" autocomplete="off">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Work in Progress To Date </label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->work_inprogress) && $post_data->work_inprogress != '' ? $post_data->work_inprogress : '' ?>" name="work_inprogress">
										</div>
									</div>
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
								
									<!--site management-->
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="m-section__sub">
											<b>Site Management Team</b>
										</div>
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th style="width:1%">No</th>
															<th>Position</th>
															<th>Name</th>
															<th>Team Name</th>
															<th style="width:1%"></th>
														</tr>
													</thead>
													<tbody class="management_body">
														<?php 
														$i = 1;
														if($id > 0 && $detail['site_management']->num_rows() > 0){
															foreach($detail['site_management']->result() as $post_data){ ?>
																<tr class="management_tr">
																	<th scope="row" class="rn"><?php echo $i ?></th>
																	<td>
																	<select class="form-control  form-control-sm " name="management_position[]">
																		<option></option>
																		<option value="manager" <?php echo isset($post_data->management_position) && $post_data->management_position == "manager" ? 'selected' : '' ?> >Project Manager</option>
																		<option value="designer" <?php echo isset($post_data->management_position) && $post_data->management_position == "designer" ? 'selected' : '' ?>>Designer</option>
																		<option value="engineer" <?php echo isset($post_data->management_position) && $post_data->management_position == "engineer" ? 'selected' : '' ?>>Project Engineer</option>
																	</select>
																	</td>
																	<td>
																	<select class="form-control  form-control-sm management_name" name="management_name[]">
																		<option></option>
																		<?php 
																		if($active_user->num_rows() > 0){
																			foreach($active_user->result() as $row_user){ ?>
																				<option value="<?php echo $row_user->id ?>" <?php echo isset($post_data->management_id) && $post_data->management_id == $row_user->id ? 'selected' : '' ?>><?php echo isset($row_user->username) && $row_user->username != '' ? $row_user->username : '' ?></option>
																		<?php
																			}
																		}
																		?>
																	</select></td>
																	<td class="team_member"><?php echo isset($detail['site_management']->user) && $detail['site_management']->user != "" ? $detail['site_management']->user : ''?></td>
																	<td><button class="btn btn-danger btn-sm btn-delete-management" type="button"><span class="la la-close"></span></button></td>
																</tr>
														<?php
															}
														}
														else{ ?>
															<tr class="management_tr">
															<th scope="row" class="rn">1</th>
															<td>
															<select class="form-control  form-control-sm " name="management_position[]">
																<option></option>
																<option value="manager">Project Manager</option>
																<option value="designer">Designer</option>
																<option value="engineer">Project Engineer</option>
															</select>
															</td>
															<td>
															<select class="form-control  form-control-sm management_name" name="management_name[]">
																<option></option>
																<?php 
																if($active_user->num_rows() > 0){
																	foreach($active_user->result() as $row_user){ ?>
																		<option value="<?php echo $row_user->id ?>"><?php echo isset($row_user->username) && $row_user->username != '' ? $row_user->username : '' ?></option>
																<?php
																	}
																}
																?>
															</select></td>
															<td class="team_member"></td>
															<td><button class="btn btn-danger btn-sm btn-delete-management" type="button"><span class="la la-close"></span></button></td>
														</tr>
														<?php
														}
														
														?>
													</tbody>
												</table>
											</div>
										</div>
										
										<button type="button" class="btn btn-info btn-sm btn-management"><i class="la la-plus"></i> Add Line</button>
									</div>
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<!--completion date-->
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="m-section__sub">
											<b>Completion Date</b>
										</div>
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th style="width:5%">Block</th>
															<th>Commencement Date</th>
															<th>Completion Date</th>
															<th>Latest EOT Date</th>
															<th>Level Quantity</th>
															<th>Unit Quantity per Level</th>
															<?php 
															if($id > 0){ ?>
																<th style="width:1%"></th>
															<?php
															}
															?>
															<th style="width:1%"></th>
														</tr>
													</thead>
													<tbody class="completion_body">
														<?php 
														if($id > 0 && $detail['completion']->num_rows() > 0){
															foreach($detail['completion']->result() as $post_data){ ?>
																<tr class="completion_tr">
																	<th scope="row" class="rn2"><input class="form-control form-control-sm m-input" type="text"  value="<?php echo isset($post_data->block) && $post_data->block != '' ? $post_data->block : '' ?>" name="completion_block[]" ></th>
																	<td><input class="form-control form-control-sm m-input m_datepicker" type="text"  value="<?php echo isset($post_data->completion_commencement) && $post_data->completion_commencement != "" ? date('Y-m-d', strtotime($post_data->completion_commencement)) : '' ?>" name="completion_commencement[]" autocomplete="off"></td>
																	<td><input class="form-control  form-control-sm m-input m_datepicker" type="text"  value="<?php echo isset($post_data->completion_date) && $post_data->completion_date != "" ? date('Y-m-d', strtotime($post_data->completion_date)) : '' ?>" name="completion_date[]" autocomplete="off"></td>
																	<td><input class="form-control form-control-sm m-input m_datepicker" type="text"  value="<?php echo isset($post_data->completion_latest_eot) && $post_data->completion_latest_eot != "" ? date('Y-m-d', strtotime($post_data->completion_latest_eot)) : '' ?>" name="completion_latest_eot[]" autocomplete="off"></td>
																	<td><input class="form-control form-control-sm m-input" type="text" value="<?php echo isset($post_data->completion_level_quantity) && $post_data->completion_level_quantity != '' ? $post_data->completion_level_quantity : ''?>" name="completion_level_quantity[]"></td>
																	<td><input class="form-control form-control-sm m-input" type="text" value="<?php echo isset($post_data->completion_unit_quantity) && $post_data->completion_unit_quantity != '' ? $post_data->completion_unit_quantity : ''?>" name="completion_unit_quantity[]"></td>
																	<td><a href="<?php echo base_url() ?>building/graph_form/<?php echo $id;?>/<?php echo $post_data->id?>" class="btn btn-info btn-sm">Setup Building Graph</a></td> 
																	<td><button class="btn btn-danger btn-sm btn-delete-completion" type="button"><span class="la la-close"></span></button></td>
																</tr>
														<?php
															}
														}
														else{ ?>
															<tr class="completion_tr">
																<th scope="row" class="rn2"><input class="form-control  form-control-sm m-input" type="text"  value="" name="completion_block[]" ></th>
																<td><input class="form-control  form-control-sm m-input m_datepicker" type="text"  value="" name="completion_commencement[]" autocomplete="off"></td>
																<td><input class="form-control  form-control-sm m-input m_datepicker" type="text"  value="" name="completion_date[]" autocomplete="off"></td>
																<td><input class="form-control  form-control-sm m-input m_datepicker" type="text"  value="" name="completion_latest_eot[]" autocomplete="off"></td>
																<td><input class="form-control  form-control-sm m-input" type="text" value="" name="completion_level_quantity[]"></td>
																<td><input class="form-control  form-control-sm m-input" type="text" value="" name="completion_unit_quantity[]"></td>
																<td><button class="btn btn-danger btn-sm btn-delete-completion" type="button"><span class="la la-close"></span></button></td>
															</tr>
														<?php
														}
														?>
													
													</tbody>
												</table>
											</div>
										</div>
										
										<button type="button" class="btn btn-info btn-sm btn-completion"><i class="la la-plus"></i> Add Line</button>
									</div>
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<!--plan Schedule-->
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="m-section__sub">
											<b>Project Planned Schedule</b>
										</div>
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th style="width:5%">Block</th>
															<th>FCF Submission Date</th>
															<th>Production Order Submission Date</th>
															<th>Team P/O Issue Date</th>
															<th>Installation Start Date</th>
															<th>Completion Date</th>
															<th>DLP End Date</th>
															<th style="width:1%"></th>
														</tr>
													</thead>
													<tbody class="planned_body">
													<?php 
													if($id > 0 && $detail['planned']->num_rows() > 0){
														foreach($detail['planned']->result() as $post_data){ ?>
															<tr class="planned_tr">
																<th scope="row" class="rn3"><input class="form-control form-control-sm  m-input" type="text"  value="<?php echo isset($post_data->block) && $post_data->block != "" ? $post_data->block : '' ?>" name="planed_block[]" autocomplete="off"></th>
																<td><input class="form-control  form-control-sm m-input m_datepicker" type="text"  value="<?php echo isset($post_data->planned_fcf_submission) && $post_data->planned_fcf_submission != "" ? date('Y-m-d',  strtotime($post_data->planned_fcf_submission)) : '' ?>" name="planned_fcf_submission[]" autocomplete="off"></td>
																<td><input class="form-control  form-control-sm m-input m_datepicker" type="text"  value="<?php echo isset($post_data->planned_production_order) && $post_data->planned_production_order != "" ? date('Y-m-d',  strtotime($post_data->planned_production_order)) : '' ?>" name="planned_production_order[]" autocomplete="off"></td>
																<td><input class="form-control  form-control-sm m-input m_datepicker" type="text"  value="<?php echo isset($post_data->planned_team_issued) && $post_data->planned_team_issued != "" ? date('Y-m-d',  strtotime($post_data->planned_team_issued)) : '' ?>" name="planned_team_issued[]" autocomplete="off"></td>
																<td><input class="form-control  form-control-sm m-input m_datepicker" type="text"  value="<?php echo isset($post_data->planned_installation_start) && $post_data->planned_installation_start != "" ? date('Y-m-d',  strtotime($post_data->planned_installation_start)) : '' ?>" name="planned_installation_start[]" autocomplete="off"></td>
																<td><input class="form-control  form-control-sm m-input m_datepicker" type="text"  value="<?php echo isset($post_data->planned_completion_date) && $post_data->planned_completion_date != "" ? date('Y-m-d',  strtotime($post_data->planned_completion_date)) : '' ?>" name="planned_completion_date[]" autocomplete="off"></td>
																<td><input class="form-control  form-control-sm m-input m_datepicker" type="text"  value="<?php echo isset($post_data->planned_dlp_end) && $post_data->planned_dlp_end != "" ? date('Y-m-d',  strtotime($post_data->planned_dlp_end)) : '' ?>" name="planned_dlp_end[]" autocomplete="off"></td>
																<td><button class="btn btn-danger btn-sm btn-delete-planned" type="button"><span class="la la-close"></span></button></td>
															</tr>
													<?php
														}
													}
													else{ ?>s
														<tr class="planned_tr">
															<th scope="row" class="rn3"><input class="form-control m-input" type="text"  value="" name="planed_block[]" autocomplete="off"></th>
															<td><input class="form-control m-input m_datepicker" type="text"  value="" name="planned_fcf_submission[]" autocomplete="off"></td>
															<td><input class="form-control m-input m_datepicker" type="text"  value="" name="planned_production_order[]" autocomplete="off"></td>
															<td><input class="form-control m-input m_datepicker" type="text"  value="" name="planned_team_issued[]" autocomplete="off"></td>
															<td><input class="form-control m-input m_datepicker" type="text"  value="" name="planned_installation_start[]" autocomplete="off"></td>
															<td><input class="form-control m-input m_datepicker" type="text"  value="" name="planned_completion_date[]" autocomplete="off"></td>
															<td><input class="form-control m-input m_datepicker" type="text"  value="" name="planned_dlp_end[]" autocomplete="off"></td>
															<td><button class="btn btn-danger btn-sm btn-delete-planned" type="button"><span class="la la-close"></span></button></td>
														</tr>
													<?php
													}
													?>
													
														
													</tbody>
												</table>
											</div>
										</div>
										
										<button type="button" class="btn btn-info btn-sm btn-planned"><i class="la la-plus"></i> Add Line</button>
									</div>
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<?php 
										if($id > 0){
											$post_data = $detail['project_setup']->row();
										}
										?>
										
										<div class="m-section__sub">
											<b>Milestone</b>
										</div>
										<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="form-group m-form__group  m-form__group--md  row">
											<label for="example-tel-input" class="col-2 col-form-label">Create Date</label>
											<div class="col-10">
												<input class="form-control  form-control-sm m-input m_datepicker" type="text" value="<?php echo isset($post_data->create_date) && $post_data->create_date != '' ? date('Y-m-d', strtotime($post_data->create_date)) : '' ?>" name="create_date">
											</div>
										</div>
										<div class="form-group m-form__group m-form__group--md  row">
											<label for="exampleTextarea" class="col-2 col-form-label">Remarks</label>
											<div class="col-10">
												<textarea class="form-control form-control-sm  m-input " name="remarks" id="exampleTextarea" rows="5"><?php echo isset($post_data->remarks) && $post_data->remarks != '' ? $post_data->remarks : '' ?></textarea>
											</div>
										</div>
									</div>
									</div>
								</div>
								

								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2">
											</div>
											<div class="col-10">
												<button type="submit" class="btn btn-sm btn-success">Submit</button>
												<button type="reset" class="btn btn-sm btn-secondary">Cancel</button>
												<button type="button" class="btn btn-sm btn-info">Link To Project Folder</button>
												<button type="button" class="btn btn-sm btn-primary">Issue FCF</button>
												<button type="button" class="btn btn-sm btn-primary">Issue Si</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
<?php $this->load->view("footer_v")?>
<script>
var baseUrl = '<?php echo base_url();?>';

var renumbering = function(){
	$.each($('.rn'), function(key, elem){
		$(elem).html(key + 1);
	});
}

$(function(){
	$(".dropdown").select2({
		placeholder: "Please Select"
	});
	
	$(".m_datepicker").datepicker({
		clearBtn: true,
		format: 'yyyy-mm-dd'
	});
	
	
	$(".m-bootstrap-select").selectpicker();
	
	$('.btn-management').on('click', function(){
		var newRow = $('.management_tr').first().clone();
		$('.management_body').append(newRow);
		
		$(".m_datepicker").datepicker({
			clearBtn: true,
			format: 'yyyy-mm-dd'
		});
	
		renumbering();
	});
	
	$('body').on('click', '.btn-delete-management', function(){
		if($('.management_tr').length != 1){
			$(this).closest('.management_tr').remove();
			renumbering();
		}
	});
	
	$('.btn-completion').on('click', function(){
		var newRow = $('.completion_tr').first().clone();
		$('.completion_body').append(newRow);
		
		$(".m_datepicker").datepicker({
			clearBtn: true,
			format: 'yyyy-mm-dd'
		});
		
	});
	
	$('body').on('click', '.btn-delete-completion', function(){
		if($('.completion_tr').length != 1){
			$(this).closest('.completion_tr').remove();
			
		}
	});
	
	$('.btn-planned').on('click', function(){
		var newRow = $('.planned_tr').first().clone();
		$('.planned_body').append(newRow);
		
		$(".m_datepicker").datepicker({
			clearBtn: true,
			format: 'yyyy-mm-dd'
		});
		
	});
	
	$('body').on('click', '.btn-delete-planned', function(){
		if($('.planned_tr').length != 1){
			$(this).closest('.planned_tr').remove();
			
		}
	});
	
	$('.management_name').on('change', function(){
		// var row = $(this).closest('tr td');
		var user_id = $(this).find(':selected').val();
		
		$.ajax({
			type: "POST",
			url: baseUrl + 'setup/get_project_team',
			dataType: 'json',
			data: {'id': user_id},
			success: function(result) {
				$('.team_member').html(result['user']);
				
				console.log(result['user']);
			}
		});
		// console.log(user_id);
	
	});
});
</script>