<?php $this->load->view("header_v")?>

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Progress Claim</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-columns"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">BQ Setup</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
						<!--begin::Portlet-->
						<div class="m-portlet m-portlet--tab">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" name="userform" method="POST" action="">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10">
										<h3 class="alert m-alert m-alert--default" role="alert">
											Bill of Quantities (BQ) Setup
										</h3>
									</div>
									<div class="form-group m-form__group m--margin-top-10">
										<?php get_msg(); ?>
									</div>
									<div class="form-group m-form__group  m-form__group--md row" >
										<label for="example-text-input" class="col-2 col-form-label">Project Name</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->project_name) && $post_data->project_name != '' ? $post_data->project_name : '' ?>" name="project_name">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Project Code</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->project_code) && $post_data->project_code != '' ? $post_data->project_code : '' ?>" name="project_code">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Type</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->type) && $post_data->type != '' ? $post_data->type : '' ?>" name="type">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Type Quantity</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->type_quantity) && $post_data->type_quantity != '' ? $post_data->type_quantity : '' ?>" name="type_quantity">
										</div>
									</div>
								
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<!--site management-->
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th rowspan="3" style="width: 1%;">Line No</th>
															<th rowspan="3" nowrap style="min-width: 160px;">Scope of Work</th>
															<th rowspan="3" nowrap style="min-width: 130px;">Location</th>
															<th rowspan="3" nowrap style="min-width: 130px;">Description</th>
															<th rowspan="3" nowrap style="min-width: 130px;">Quantity</th>
															<th rowspan="3" nowrap style="min-width: 130px;">Rate (RM)</th>
															<th rowspan="3" nowrap style="min-width: 130px;">Total (RM)</th>
															<th colspan="9" style="text-align: center">Work Done Percentage Setup</th>
															<th rowspan="3" nowrap style="min-width: 130px;">Remark</th>
															<th rowspan="3" nowrap style="min-width: 140px;"></th>
														</tr>
														<tr>
															<th colspan="4" style="text-align: center">For Cabinet Installation</th>
															<th colspan="5" style="text-align: center">Individual Installation</th>
														</tr>
														<tr>
															<th  style="min-width: 100px;">Delivery of Material</th>
															<th  style="min-width: 100px;">Distribution of Material</th>
															<th  style="min-width: 100px;">Carcass Assembly</th>
															<th  style="min-width: 100px;">Door &Panel Installation</th>
															<th  style="min-width: 100px;">Worktop</th>
															<th  style="min-width: 100px;">Glass/Mirror</th>
															<th  style="min-width: 100px;">Appliances</th>
															<th  style="min-width: 100px;">Sub-Contact</th>
															<th  style="min-width: 100px;">ID Works</th>
														</tr>
													</thead>
													<tbody class="type_body">
														<tr class="type_tr">
															<th scope="row" class="rn">1</th>
															<td>
															<select class="form-control m-input " name="scope_of_work[]">
																<option></option>
																<?php 
																foreach(scope_work() as $scope_work){ ?>
																<option value="<?php echo $scope_work?>" ><?php echo $scope_work ?></option>
																<?php
																}
																?>
															</select>
															</td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="location[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="description[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="quantity[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="rate[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="total[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="del_material[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="dis_material[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="carcass[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="door_panel[]"></td>
															<td><input class="form-control form-control-sm m-input" type="text" value="" name="worktop[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="glass[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="appliance[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="subcont[]"></td>
															<td><input class="form-control  form-control-sm m-input" type="text" value="" name="id_work[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="remark[]"></td>
															<td>
															<a href="<?php echo base_url() ?>claim/bq_vo" class="btn btn-sm btn-success" title="VO"><span class="la la-file-text"></span></a>
															<button type="button" class="btn btn-sm btn-info" title="View VO History"><span class="la la-history"></span></button>
															<button type="button" class="btn btn-sm btn-danger btn-delete-type" title="Delete"><span class="la la-close"></span></button>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<button type="button" class="btn btn-sm btn-info btn-type"><i class="la la-plus"></i> Add Line</button>
									</div>
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="form-group m-form__group  m-form__group--md row">
											<label for="example-tel-input" class="col-2 col-form-label">Create Date</label>
											<div class="col-10">
												<input class="form-control  form-control-sm m-input m_datepicker" type="text" value="<?php echo isset($post_data->create_date) && $post_data->create_date != '' ? $post_data->create_date : '' ?>" name="create_date">
											</div>
										</div>
										<div class="form-group m-form__group  m-form__group--md row">
											<label for="exampleTextarea" class="col-2 col-form-label">Remarks</label>
											<div class="col-10">
												<textarea class="form-control  form-control-sm m-input" name="remarks" id="exampleTextarea" rows="5"></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2">
											</div>
											<div class="col-10">
												<button type="submit" class="btn btn-sm btn-success">Submit</button>
												<button type="reset" class="btn btn-sm btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
<?php $this->load->view("footer_v")?>
<script>
var renumbering = function(){
	$.each($('.rn'), function(key, elem){
		$(elem).html(key + 1);
	});
}

$(function(){
	$(".dropdown").select2({
		placeholder: "Please Select"
	});
	
	$(".m_datepicker").datepicker({
		clearBtn: true,
		format: 'dd/mm/yyyy'
	});
	
	$('.btn-type').on('click', function(){
		var newRow = $('.type_tr').first().clone();
		$('.type_body').append(newRow);
		
		renumbering();
	});
	
	$('body').on('click', '.btn-delete-type', function(){
		if($('.type_tr').length != 1){
			$(this).closest('.type_tr').remove();
			
			renumbering();
		}
	});
});
</script>