<?php $this->load->view("header_v")?>

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Progress Claim</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-columns"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">BQ vo</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
						<!--begin::Portlet-->
						<div class="m-portlet m-portlet--tab">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" name="userform" method="POST" action="">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10">
										<h3 class="alert m-alert m-alert--default" role="alert">
											Bill of Quantities (BQ) VO
										</h3>
									</div>
									<div class="form-group m-form__group m--margin-top-10">
										<?php get_msg(); ?>
									</div>
									<div class="form-group m-form__group m-form__group--md  row" >
										<label for="example-text-input" class="col-2 col-form-label">Project Name</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->project_name) && $post_data->project_name != '' ? $post_data->project_name : '' ?>" name="project_name">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Project Code</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->project_code) && $post_data->project_code != '' ? $post_data->project_code : '' ?>" name="project_code">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Type</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->type) && $post_data->type != '' ? $post_data->type : '' ?>" name="type">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Type Quantity</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->type_quantity) && $post_data->type_quantity != '' ? $post_data->type_quantity : '' ?>" name="type_quantity">
										</div>
									</div>
								
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<!--site management-->
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="m-section__sub">
											<b>Original Contract</b>
										</div>
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th>Line</th>
															<th>Scope of Work</th>
															<th>Location</th>
															<th>Description</th>
															<th>Quantity</th>
															<th>Rate (RM)</th>
															<th>Total (RM)</th>
															<th>Updated Date</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th scope="row" >1</th>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr class="type_tr">
															<th scope="row" colspan="6" style="text-align: right;">Total (Omission/Addition) </th>
															<td colspan="2"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="m-section__sub">
											<b>VO</b>
										</div>
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th>VO Reference No</th>
															<th>Scope of Work</th>
															<th>Location</th>
															<th>Description</th>
															<th>Quantity</th>
															<th>Rate (RM)</th>
															<th>Total (RM)</th>
															<th>Remark</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th scope="row" ><input class="form-control form-control-sm  m-input" type="text" value="" name="vo_ref_no"></th>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="scope"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="location"></td>
															<td><input class="form-control form-control-sm m-input" type="text" value="" name="desc"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="qty"></td>
															<td><input class="form-control  form-control-sm m-input" type="text" value="" name="rate"></td>
															<td><input class="form-control  form-control-sm m-input" type="text" value="" name="total"></td>
															<td><input class="form-control  form-control-sm m-input" type="text" value="" name="remark"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="form-group m-form__group m-form__group--md  row">
											<label for="example-tel-input" class="col-2 col-form-label">Last Updated Date</label>
											<div class="col-10">
												<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->contract_description) && $post_data->contract_description != '' ? $post_data->contract_description : '' ?>" name="contract_description">
											</div>
										</div>
										<div class="form-group m-form__group  m-form__group--md row">
											<label for="exampleTextarea" class="col-2 col-form-label">Remarks</label>
											<div class="col-10">
												<textarea class="form-control  form-control-sm m-input" name="remarks" id="exampleTextarea" rows="5"></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2">
											</div>
											<div class="col-10">
												<button type="submit" class="btn btn-sm btn-success">Submit</button>
												<button type="reset" class="btn btn-sm btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
<?php $this->load->view("footer_v")?>
<script>
$(function(){
	$(".dropdown").select2({
		placeholder: "Please Select"
	});
	
	$(".m_datepicker").datepicker({
		clearBtn: true,
		format: 'dd/mm/yyyy'
	});
});
</script>