<?php $this->load->view("header_v")?>

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Progress Claim</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-columns"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">New Progress Claim</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
						<!--begin::Portlet-->
						<div class="m-portlet m-portlet--tab">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" name="userform" method="POST" action="">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10">
										<h3 class="alert m-alert m-alert--default" role="alert">
											Progress Claim
										</h3>
									</div>
									<div class="form-group m-form__group m--margin-top-10">
										<?php get_msg(); ?>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="" class="col-2 col-form-label">Progress Claim No</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->progress_claim_no) && $post_data->progress_claim_no != '' ? $post_data->progress_claim_no : '' ?>" name="progress_claim_no">
										</div>	
									</div>
									<div class="form-group m-form__group  m-form__group--md row" >
										<label for="example-text-input" class="col-2 col-form-label">Project Name</label>
										<div class="col-10">
											<select class="form-control form-control-sm  m-input dropdown" name="project_name">
												<option></option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Project Code</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->project_code) && $post_data->project_code != '' ? $post_data->project_code : '' ?>" name="project_code">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Client Name</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->client_name) && $post_data->client_name != '' ? $post_data->client_name : '' ?>" name="client_name">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Name PIC</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->pic) && $post_data->pic != '' ? $post_data->pic : '' ?>" name="pic">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Contact No</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->contact_no) && $post_data->contact_no != '' ? $post_data->contact_no : '' ?>" name="contact_no">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Overall Total Units</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->total_unit) && $post_data->total_unit != '' ? $post_data->total_unit : '' ?>" name="total_unit">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Contract Amount</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->contract_amount) && $post_data->contract_amount != '' ? $post_data->contract_amount : '' ?>" name="contract_amount">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">LOA Received Date</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->received_date) && $post_data->received_date != '' ? $post_data->received_date : '' ?>" name="received_date">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Contract Desciption</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->contract_description) && $post_data->contract_description != '' ? $post_data->contract_description : '' ?>" name="contract_description">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">VO Amount</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->vo_amount) && $post_data->vo_amount != '' ? $post_data->vo_amount : '' ?>" name="vo_amount">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Previous Claimed Amount</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->previous_claimed_amount) && $post_data->previous_claimed_amount != '' ? $post_data->previous_claimed_amount : '' ?>" name="previous_claimed_amount">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Workdone To date</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->retention_sum) && $post_data->retention_sum != '' ? $post_data->retention_sum : '' ?>" name="retention_sum">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Retention Sum </label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->retention_sum) && $post_data->retention_sum != '' ? $post_data->retention_sum : '' ?>" name="retention_sum">
										</div>
									</div>
									
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<!--site management-->
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="m-section__sub">
											<b>Summary of Workdone</b>
										</div>
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th style="width:1%">No</th>
															<th>Scope of Work</th>
															<th>Amount</th>
															<th>Workdone % </th>
															<th>Workdone (RM)</th>
														</tr>
													</thead>
													<tbody>
														<?php 
														$i = 1;
														foreach(scope_work() as $scope_work){ ?>
														<tr>
															<th scope="row"><?php echo $i?></th>
															<td><?php echo $scope_work ?></td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<?php
														$i++;
														}
														?>
														<tr>
															<th colspan="2" style="text-align: right;">Total</th>
															<td></td>
															<td></td>
															<td></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="form-group m-form__group  m-form__group--md row">
											<label for="example-tel-input" class="col-2 col-form-label">Create Date</label>
											<div class="col-10">
												<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->contract_description) && $post_data->contract_description != '' ? $post_data->contract_description : '' ?>" name="contract_description">
											</div>
										</div>
										<div class="form-group m-form__group m-form__group--md  row">
											<label for="exampleTextarea" class="col-2 col-form-label">Remarks</label>
											<div class="col-10">
												<textarea class="form-control  form-control-sm m-input" name="remarks" id="exampleTextarea" rows="5"></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2">
											</div>
											<div class="col-10">
												<button type="submit" class="btn btn-sm btn-success">Submit</button>
												<button type="reset" class="btn btn-sm btn-secondary">Cancel</button>
												<button type="button" class="btn btn-sm btn-info">Print Progress Claim</button>
												<button type="button" class="btn btn-sm btn-info">Print Workdone Summary</button>
												<button type="button" class="btn btn-sm btn-info">Print Workdone Details</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
<?php $this->load->view("footer_v")?>
<script>
$(function(){
	$(".dropdown").select2({
		placeholder: "Please Select"
	});
	
	$(".m_datepicker").datepicker({
		clearBtn: true,
		format: 'dd/mm/yyyy'
	});
	
});
</script>