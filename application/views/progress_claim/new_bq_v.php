<?php $this->load->view("header_v")?>

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Progress Claim</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-columns"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">New BQ</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
						<!--begin::Portlet-->
						<div class="m-portlet m-portlet--tab">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" name="userform" method="POST" action="">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10">
										<h3 class="alert m-alert m-alert--default" role="alert">
											Bill of Quantities (BQ)
										</h3>
									</div>
									<div class="form-group m-form__group m--margin-top-10">
										<?php get_msg(); ?>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="" class="col-2 col-form-label">BQ No</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->bq_no) && $post_data->bq_no != '' ? $post_data->bq_no : '' ?>" name="bq_no">
										</div>	
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="" class="col-2 col-form-label">BQ Type</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->bq_type) && $post_data->bq_type != '' ? $post_data->bq_type : '' ?>" name="bq_type">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row" >
										<label for="example-text-input" class="col-2 col-form-label">Project Name</label>
										<div class="col-10">
											<select class="form-control  form-control-sm m-input " name="project_name">
												<option></option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Project Code</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->project_code) && $post_data->project_code != '' ? $post_data->project_code : '' ?>" name="project_code">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Client Name</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->client_name) && $post_data->client_name != '' ? $post_data->client_name : '' ?>" name="client_name">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Name PIC</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->pic) && $post_data->pic != '' ? $post_data->pic : '' ?>" name="pic">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Contact No</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->contact_no) && $post_data->contact_no != '' ? $post_data->contact_no : '' ?>" name="contact_no">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Overall Total Units</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->total_unit) && $post_data->total_unit != '' ? $post_data->total_unit : '' ?>" name="total_unit">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Contract Amount</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->contract_amount) && $post_data->contract_amount != '' ? $post_data->contract_amount : '' ?>" name="contract_amount">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">LOA Received Date</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->received_date) && $post_data->received_date != '' ? $post_data->received_date : '' ?>" name="loa_received_date">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Contract Desciption</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->contract_description) && $post_data->contract_description != '' ? $post_data->contract_description : '' ?>" name="contract_description">
										</div>
									</div>
								
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<!--site management-->
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="m-section__sub">
											<b>Define Types</b>
										</div>
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th style="width:1%">No</th>
															<th>Type</th>
															<th>QTY</th>
															<th>Assign QTY</th>
															<th>Updated Amount</th>
															<th>VO Amount</th>
															<th style="width: 15%">Action</th>
														</tr>
													</thead>
													<tbody class="type_body">
														<tr class="type_tr">
															<th scope="row" class="rn">1</th>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="type[]"></td>
															<td><input class="form-control  form-control-sm m-input" type="text" value="" name="qty[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="assign_qty[]"></td>
															<td><input class="form-control  form-control-sm m-input" type="text" value="" name="updated_amount[]"></td>
															<td><input class="form-control form-control-sm  m-input" type="text" value="" name="vo_amount[]"></td>
															<td>
															<a href="<?php echo base_url()?>claim/bq_setup" class="btn btn-primary m-btn m-btn--icon btn-sm m-btn--icon-only" title="Setup"><i class="la la-cogs"></i></a>
															<button type="button" class="btn btn-secondary m-btn m-btn--icon btn-sm m-btn--icon-only" title="Edit"><i class="la la-edit"></i></button>
															<button type="button" class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only" title="View"><i class="la la-eye"></i></button>
															<button type="button" class="btn btn-accent m-btn m-btn--icon btn-sm m-btn--icon-only" title="Duplicate"><i class="la la-clipboard"></i></button>
															<button type="button" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only btn-delete-type" title="Delete"><i class="la la-close"></i></button>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<button type="button" class="btn btn-sm btn-info btn-type"><i class="la la-plus"></i> Add New Type</button>
									</div>
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="form-group m-form__group  m-form__group--md row">
											<label for="example-tel-input" class="col-2 col-form-label">Create Date</label>
											<div class="col-10">
												<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->contract_description) && $post_data->contract_description != '' ? $post_data->contract_description : '' ?>" name="contract_description">
											</div>
										</div>
										<div class="form-group m-form__group  m-form__group--md row">
											<label for="exampleTextarea" class="col-2 col-form-label">Remarks</label>
											<div class="col-10">
												<textarea class="form-control form-control-sm  m-input" name="remarks" id="exampleTextarea" rows="5"></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2">
											</div>
											<div class="col-10">
												<button type="submit" class="btn btn-sm btn-success">Submit</button>
												<a href="<?php echo base_url() ?>setup/project_setup" class="btn btn-sm btn-secondary">Back</a>
												<button type="button" class="btn btn-sm btn-info">Save and keep as original BQ</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
<?php $this->load->view("footer_v")?>
<script>
var renumbering = function(){
	$.each($('.rn'), function(key, elem){
		$(elem).html(key + 1);
	});
}

$(function(){
	$(".dropdown").select2({
		placeholder: "Please Select"
	});
	
	$(".m_datepicker").datepicker({
		clearBtn: true,
		format: 'dd/mm/yyyy'
	});
	
	$('.btn-type').on('click', function(){
		var newRow = $('.type_tr').first().clone();
		$('.type_body').append(newRow);
		
		renumbering();
	});
	
	$('body').on('click', '.btn-delete-type', function(){
		if($('.type_tr').length != 1){
			$(this).closest('.type_tr').remove();
			
			renumbering();
		}
	});
});
</script>