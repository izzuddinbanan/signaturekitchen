<?php $this->load->view("header_v")?>

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Progress Payment</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-money"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">New Subcont BQ</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
						<!--begin::Portlet-->
						<div class="m-portlet m-portlet--tab">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" name="userform" method="POST" action="">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10">
										<h3 class="alert m-alert m-alert--default" role="alert">
											Bill of Quantities - SubCont BQ
										</h3>
									</div>
									<div class="form-group m-form__group m--margin-top-10">
										<?php get_msg(); ?>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="" class="col-2 col-form-label">Subcont BQ No</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="<?php echo isset($post_data->subcont_bq_no) && $post_data->subcont_bq_no != '' ? $post_data->subcont_bq_no : '' ?>" name="subcont_bq_no">
										</div>	
									</div>
									<div class="form-group m-form__group  m-form__group--md row" >
										<label for="example-text-input" class="col-2 col-form-label">Project Name</label>
										<div class="col-10">
											<select class="form-control m-input  form-control-sm " name="project_name">
												<option></option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Project Code</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->project_code) && $post_data->project_code != '' ? $post_data->project_code : '' ?>" name="project_code">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Overall Total Units</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->total_unit) && $post_data->total_unit != '' ? $post_data->total_unit : '' ?>" name="total_unit">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">LOA Received Date</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->loa_received_date) && $post_data->loa_received_date != '' ? $post_data->loa_received_date : '' ?>" name="loa_received_date">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Target Completion Date</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->completion_date) && $post_data->completion_date != '' ? $post_data->completion_date : '' ?>" name="completion_date">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Subcont</label>
										<div class="col-10">
											<select class="form-control  form-control-sm m-input " name="subcont">
												<option></option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">GST Registration No</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->gst_no) && $post_data->gst_no != '' ? $post_data->gst_no : '' ?>" name="gst_no">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Commencement Date</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->commencement_date) && $post_data->commencement_date != '' ? $post_data->commencement_date : '' ?>" name="commencement_date">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Estimated End Date</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->estimate_end_date) && $post_data->estimate_end_date != '' ? $post_data->estimate_end_date : '' ?>" name="estimate_end_date">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Actual Start Date</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->actual_start_date) && $post_data->actual_start_date != '' ? $post_data->actual_start_date : '' ?>" name="actual_start_date">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Actual End Date</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->actual_end_date) && $post_data->actual_end_date != '' ? $post_data->actual_end_date : '' ?>" name="actual_end_date">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Project Duration</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->project_duration) && $post_data->project_duration != '' ? $post_data->project_duration : '' ?>" name="project_duration">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Defect Liability Period</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->liability_period) && $post_data->liability_period != '' ? $post_data->liability_period : '' ?>" name="liability_period">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Liquid & Ascertained Damages </label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->liquid_damages) && $post_data->liquid_damages != '' ? $post_data->liquid_damages : '' ?>" name="liquid_damages">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">% of Certified Value Retained </label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->value_retained) && $post_data->value_retained != '' ? $post_data->value_retained : '' ?>" name="value_retained">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Limit of Retention Fund </label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->retention_fund) && $post_data->retention_fund != '' ? $post_data->retention_fund : '' ?>" name="retention_fund">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Payment Terms</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->payment_term) && $post_data->payment_term != '' ? $post_data->payment_term : '' ?>" name="payment_term">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Contract Description</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->contract_description) && $post_data->contract_description != '' ? $post_data->contract_description : '' ?>" name="contract_description">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Contract Remark</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="<?php echo isset($post_data->contract_remark) && $post_data->contract_remark != '' ? $post_data->contract_remark : '' ?>" name="contract_remark">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Progress Update Calculation Method </label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->calculation_method) && $post_data->calculation_method != '' ? $post_data->calculation_method : '' ?>" name="calculation_method">
										</div>
									</div>
									
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<!--site management-->
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="m-section__sub">
											<b>BQ Overview</b>
										</div>
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr>
															<th style="width:1%">Type</th>
															<th style="width:1%">Assigned Units QTY</th>
															<th>Item No</th>
															<th>Subcont Scope of Work</th>
															<th>Description</th>
															<th>Quantity /  Unit</th>
															<th>UOM</th>
															<th>Rate (RM)</th>
															<th>Total Amount (RM)</th>
															<th>Total SI Amount (RM)</th>
															<th>Total Omission Amount (RM)</th>
															<th>Total Additional Amount (RM)</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th scope="row">1</th>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<th colspan="8" style="text-align: right;">Total</th>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<th colspan="8" style="text-align: right;">Lumpsum Discount</th>
															<td colspan="4"></td>
														</tr>
														<tr>
															<th colspan="8" style="text-align: right;">Contract Amount </th>
															<td colspan="4"></td>
														</tr>
														<tr>
															<th colspan="8" style="text-align: right;">Deposit</th>
															<td colspan="4"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="form-group m-form__group m-form__group--md  row">
											<label for="example-tel-input" class="col-2 col-form-label">Create Date</label>
											<div class="col-10">
												<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->create_date) && $post_data->create_date != '' ? $post_data->create_date : '' ?>" name="create_date">
											</div>
										</div>
										<div class="form-group m-form__group  m-form__group--md row">
											<label for="exampleTextarea" class="col-2 col-form-label">Remarks</label>
											<div class="col-10">
												<textarea class="form-control  form-control-sm m-input" name="remarks" id="exampleTextarea" rows="5"></textarea>
											</div>
										</div>
										<div class="form-group m-form__group  m-form__group--md row">
											<label for="example-tel-input" class="col-2 col-form-label">PO Reference No</label>
											<div class="col-10">
												<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($post_data->po_reference_no) && $post_data->po_reference_no != '' ? $post_data->po_reference_no : '' ?>" name="po_reference_no">
											</div>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2">
											</div>
											<div class="col-10">
												<button type="submit" class="btn btn-sm btn-success">Submit</button>
												<button type="reset" class="btn btn-sm btn-secondary">Cancel</button>
												<button type="button" class="btn btn-sm btn-info">Print Form Of PO Preparation</button>
												<button type="button" class="btn btn-sm btn-info">Print BQ</button>
												<button type="button" class="btn btn-sm btn-primary">Setup Subcont BQ</button>
												<button type="button" class="btn btn-sm btn-primary">View Subcont BQ</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
						
						
<?php $this->load->view("footer_v")?>
<script>
$(function(){
	$(".dropdown").select2({
		placeholder: "Please Select"
	});
});
</script>