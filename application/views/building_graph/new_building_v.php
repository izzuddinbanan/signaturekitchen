<?php $this->load->view("header_v")?>
<?php 
$project_id = $this->uri->segment(3);
$completion_id = $this->uri->segment(4);
$building_id = $this->uri->segment(5);
// if($id > 0){
	// $post_data = $graph_detail['building']->row();
// }
?>
					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Building Graph</h3>
								 <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-cogs"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">New Building Graph</span>
										</a>
									</li>
									<!-- <li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Timesheet</span>
										</a>
									</li> --> 
								</ul>
							</div>
						</div>
					</div>

					<!-- END: Subheader -->

					<div class="m-content">
						<!--begin::Portlet-->
						<div class="m-portlet m-portlet--tab">
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" name="userform" method="POST" action="<?php echo base_url()?>building/graph_form/<?php echo $project_id."/". $completion_id."/". $building_id ?>">
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10">
										<h3 class="alert m-alert m-alert--default" role="alert">
											Building Graph
										</h3>
									</div>
									<div class="form-group m-form__group m--margin-top-10">
										<?php get_msg(); ?>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="" class="col-2 col-form-label">Project Name</label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($project_setup->project_name) && $project_setup->project_name != '' ? $project_setup->project_name : '' ?>" name="project_name" readonly>
										</div>	
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="" class="col-2 col-form-label">Project Code </label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($project_setup->project_code) && $project_setup->project_code != '' ? $project_setup->project_code : '' ?>" name="project_code" readonly>
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row" >
										<label for="example-text-input" class="col-2 col-form-label">Block Name </label>
										<div class="col-10">
											<input class="form-control  form-control-sm m-input" type="text" value="<?php echo isset($project_setup->block) && $project_setup->block != '' ? $project_setup->block : '' ?>" name="block_name" readonly>
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Total Assigned Units </label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="" name="total_assign_unit">
										</div>
									</div>
									<div class="form-group m-form__group  m-form__group--md row">
										<label for="example-tel-input" class="col-2 col-form-label">Total Batches</label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="" name="total_branch">
										</div>
									</div>
									<div class="form-group m-form__group m-form__group--md  row">
										<label for="example-tel-input" class="col-2 col-form-label">Building Graph Revision </label>
										<div class="col-10">
											<input class="form-control form-control-sm  m-input" type="text" value="" name="graph_revision">
										</div>
									</div>
								
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<!--site management-->
									<div class="m-section m--margin-left-30 m--margin-right-30">
										<div class="m-section__content">	
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead style="background: #f7f8fa;">
														<tr id="thead">
															<th style="min-width: 100px; width: 1%;">Level</th>
														</tr>
													</thead>
													<tbody >
														<?php 
														for($i = 1; $i <= $project_setup->completion_level_quantity; $i++){ 
															$building = $this->building_m->get_building_graph("query", array('project_id' => $project_id, 'project_completion_id' => $completion_id, 'level' => $i));
														?>
															<tr class="tbody">
																<td><input type="text" class="form-control  form-control-sm level" value="<?php echo $i ?>" name="level[]" autocomplete="off" ></td>
																<?php 
																$total_column = 0;
																if($building->num_rows() > 0){
																	foreach($building->result() as $row_setting){ ?>
																	<td><input type="text" class="form-control  form-control-sm unit" value="<?php echo isset($row_setting->unit) && $row_setting->unit != '' ? $row_setting->unit : '' ?>" name="unit[<?php echo $i ?>][]" autocomplete="off"></td>
																	<td><input type="text" class="form-control  form-control-sm type-table" value="<?php echo isset($row_setting->type) && $row_setting->type != '' ? $row_setting->type : '' ?>" name="type[<?php echo $i ?>][]" autocomplete="off"></td>
																	<td><input type="text" class="form-control  form-control-sm batch-table" value="<?php echo isset($row_setting->batch) && $row_setting->batch != '' ? $row_setting->batch : '' ?>" name="batch[<?php echo $i ?>][]" autocomplete="off"></td>
																<?php
																	$total_column++;
																	}
																}
																else{ 
																	$total_column = 1;
																?>
																	<td><input type="text" class="form-control  form-control-sm unit" value="<?php echo isset($project_setup->block) && $project_setup->block != '' ? $project_setup->block ."-" .$i."-1" : '' ?>" name="unit[<?php echo $i ?>][]" autocomplete="off"></td>
																	<td><input type="text" class="form-control  form-control-sm type-table" value="<?php echo isset($row_setting->type) && $row_setting->type != '' ? $row_setting->type : '' ?>" name="type[<?php echo $i ?>][]" autocomplete="off"></td>
																	<td><input type="text" class="form-control  form-control-sm batch-table" value="<?php echo isset($row_setting->batch) && $row_setting->batch != '' ? $row_setting->batch : '' ?>" name="batch[<?php echo $i ?>][]" autocomplete="off"></td>
																<?php
																}
																?>
																
																	<input type="hidden" value="<?php echo $total_column; ?>" class="total_column"/>
															</tr>
														<?php 
														}
														
														?>
													</tbody>
												</table>
												<!-- <button type="button" class="btn btn-sm btn-info btn-add-line"><i class="la la-plus"></i> Add Line</button> -->
											</div>
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2">
											</div>
											<div class="col-10">
												<button type="submit" class="btn btn-sm btn-success">Submit</button>
												<button type="reset" class="btn btn-sm btn-secondary">Cancel</button>
												<button type="button" class="btn btn-sm btn-info">View Original Building Graph</button>
												<button type="button" class="btn btn-sm btn-primary" id="assign_type">Assign Type</button>
												<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#batch_modal">Assign Batch</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					
					<!--begin::Modal Type-->
					<div class="modal fade" id="type_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Assign Type</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label for="type" class="form-control-label">Type:</label>
										<input type="text" class="form-control" value="" autocomplete="off" id="type">
									</div>
									<div class="form-group">
										<label for="level_start" class="form-control-label">Level Start:</label>
										<input type="number" class="form-control" value="" autocomplete="off" id="level-type-start">
									</div>
									<div class="form-group">
										<label for="level_end" class="form-control-label">Level End:</label>
										<input type="number" class="form-control" value="" autocomplete="off" id="level-type-end">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary type-apply" id="type-apply">Apply</button>
								</div>
							</div>
						</div>
					</div>
					
					<!--begin::Modal batch-->
					<div class="modal fade" id="batch_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Assign Batch</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label for="batch" class="form-control-label">Batch:</label>
										<input type="text" class="form-control" value="" autocomplete="off" id="batch">
									</div>
									<div class="form-group">
										<label for="level_start" class="form-control-label">Level Start:</label>
										<input type="number" class="form-control" value="" autocomplete="off" id="level-batch-start">
									</div>
									<div class="form-group">
										<label for="level_end" class="form-control-label">Level End:</label>
										<input type="number" class="form-control" value="" autocomplete="off" id="level-batch-end">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary batch-apply" id="type-apply">Apply</button>
								</div>
							</div>
						</div>
					</div>
					<!--end::Modal-->
<?php $this->load->view("footer_v")?>
<script>
function table_header(){
	var total_column = $('.total_column').val();
	var header = '<th style="min-width: 130px;">Unit</th>' +
					'<th style="min-width: 130px;">Type</th>' +
					'<th style="min-width: 130px;">Batch</th>';
	
	for(i = 0; i < total_column; i++){
		$('#thead').append(header);
	}
}

$(function(){
	table_header();
	
	$(".dropdown").select2({
		placeholder: "Please Select"
	});
	
	$('.btn-add-line').on('click', function(){
		var newRow = $('.tr-building').first().clone();

		newRow.find('.level, .unit, .type-table, .batch-table').val("");
		
		$('.body-building').append(newRow);
	});
	
	$('body').on('click', '.btn-delete-row', function(){
		if($('.tr-building').length != 1){
			$(this).closest('.tr-building').remove();
		}
	});
	
	$('.type-apply').on('click', function(){
		var type = $('#type').val();
		var level_start = parseInt($('#level-type-start').val());
		var level_end = parseInt($('#level-type-end').val());
		
		$( ".level" ).each(function() {
			console.log($(this).val());
			if($(this).val() >= level_start && $(this).val() <= level_end){
				var row = $(this).closest('tr');
				var type_val = row.find('.type-table').val(type);
			}
		});
		
		$('#type_modal').modal('hide');
	});
	
	$('.batch-apply').on('click', function(){
		var batch = $('#batch').val();
		var level_start = parseInt($('#level-batch-start').val());
		var level_end = parseInt($('#level-batch-end').val());
		
		$( ".level" ).each(function() {
			if($(this).val() >= level_start && $(this).val() <= level_end){
				var row = $(this).closest('tr');
				var batch_val = row.find('.batch-table').val(batch);
			}
		});
		
		$('#batch_modal').modal('hide');
	});
	
	$('#assign_type').on('click', function(){
		$( ".level" ).each(function() {
			var level = $(this).val();
			var row = $(this).closest('tr');
			var body = '<td><input type="text" class="form-control form-control-sm unit" value="" name="unit[' + level + '][]" autocomplete="off"></td>' +
						'<td><input type="text" class="form-control form-control-sm type-table" value="" name="type[' + level + '][]" autocomplete="off"></td>' +
						'<td><input type="text" class="form-control form-control-sm batch-table" value="" name="batch[' + level + '][]" autocomplete="off"></td>';
						
			row.append(body);
		});
		
		var header = '<th style="min-width: 130px;">Unit</th>' +
						'<th style="min-width: 130px;">Type</th>' +
						'<th style="min-width: 130px;">Batch</th>';
						
		
		$('#thead').append(header);
	});
	
	
});
</script>