<?php
/**
* To check admin has session or not.
* To prevent admin going to Login page when there is session
*/
if(!function_exists('user_invalidate')){
	function user_invalidate(){
		$CI =& get_instance();
		if($CI->session->userdata('email') !== NULL){
			redirect('dashboard');
		}
	}
}

/**
* To check admin has session or not.
* To prevent admin going to the page that need session
*/
if(!function_exists('user_validate')){
	function user_validate(){
		$CI =& get_instance();
		$CI->load->model("login_m");
		
		$sql = $CI->login_m->user_validate();
		
		if($CI->session->userdata('email') === NULL){
			redirect('login');
		}
		else if($sql->active != '1'){
			redirect('login');
		}
		else if($CI->session->userdata('email') !== $sql->email || $CI->session->userdata('id') !== $sql->id || $CI->session->userdata('username') !== $sql->username){
			$session_data = array(
				'id' => isset($row->id) && $row->id != "" ? $row->id : "",
				'username' => isset($row->username) && $row->username != "" ? $row->username : "",
				'email' => isset($row->email) && $row->email != "" ? $row->email : "",
				'logged_in' => TRUE, 
			);
			
			$CI->session->set_userdata($session_data);
		}
	}
}


// array debug
if(!function_exists("ad")) {
	function ad($data, $var_dump=false) {
		echo '<pre>';
		if($var_dump === false) {
			print_r($data);
		} else {
			var_dump($data);
		}
		echo '</pre>';
	}
}

if(!function_exists('set_msg')){
	function set_msg($message = "", $type = "danger"){
		$CI =& get_instance();
		if($message != ""){
			$CI->session->set_userdata('error-message', $message);
			$CI->session->set_userdata('error-type', $type);
			return true;
		}
		
		return false;
	}
}

if(!function_exists('get_msg')){
	function get_msg(){
		$CI =& get_instance();
		if($CI->session->has_userdata('error-message')){
			echo '<div class="m-alert m-alert--outline alert alert-' . $CI->session->userdata('error-type') . ' alert-dismissible animated fadeIn" role="alert">		
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>			
					<span>'.$CI->session->userdata('error-message').'</span>		
				</div>';
			
			$CI->session->unset_userdata('error-message');
			$CI->session->unset_userdata('error-type');
		}
	}
}

if(!function_exists('base64url_encode')) {
	function base64url_encode($data) { 
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
	}
}

if(!function_exists('base64url_decode')) {
	function base64url_decode($data) { 
	  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
	}
}

if(!function_exists('role_setting')) {
	function role_setting($page_access="") { 
		$CI =& get_instance();
		$data =  new stdClass();
		
		$user_role = $CI->session->userdata("role");
		$user_id = $CI->session->userdata("id");
		
		if(is_numeric($user_role) && $user_role > 0){
			$sql_user_role = "SELECT * FROM `user` WHERE `id` = " .$user_id. " LIMIT 1";
			$query_user_role = $CI->db->query($sql_user_role);
			if($query_user_role->num_rows() > 0){
				$row_user_role = $query_user_role->row();
				
				$sql_get_role = "SELECT * FROM `role_privilege` WHERE `role_id` = " .$CI->db->escape($row_user_role->role). " AND `page_access_id` = ".$CI->db->escape($page_access). " LIMIT 1";
				$query_get_role = $CI->db->query($sql_get_role);
				if($query_get_role->num_rows() > 0){
					$data = $query_get_role->row();
				}
			}
		}
		
		return $data;
	}
}

if(!function_exists('scope_work')) {
	function scope_work() { 
		return array(
			'Kitchen',
			'Wardrobe',
			'Vanity',
			'Appliances',
			'Worktop',
			'Wall Cladding',
			'Loose Furniture',
			'ID Fit Out Works',
		);
	}
}

if(!function_exists('subcont_work')) {
	function subcont_work() { 
		return array(
			'Wall Cladding',
			'Worktop Fabricator',
			'ID Fit Out Works',
		);
	}
}

if(!function_exists('contractor_services')) {
	function contractor_services() { 
		return array(
			'Appliances Installer',
			'Ducting Work',
			'Plumbing Work',
			'Installer',
			'Electrician',
			'Gas technician',
			'General Worker',
		);
	}
}
?>