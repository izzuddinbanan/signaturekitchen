<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup_m extends CI_Model 
{
	public $page = array();
	function __construct() 
	{
		parent::__construct();
	}
	
	#user listing start
	private function user_validate_encrypted_data($encrypted_data = ""){
		$where = "";
		$search_data = array();
		if($encrypted_data == ""){
			$encrypted_data = base64url_encode(serialize(array()));
			redirect(base_url().'setup/user/'.$encrypted_data);
		}
		else{
			$search_data = unserialize(base64url_decode($encrypted_data));
			if(is_array($search_data)){
				$where = $this->user_search($search_data);
			}
			else{
				$encrypted_data = base64url_encode(serialize(array()));
				redirect(base_url().'setup/user/'.$encrypted_data);
			}
		}
		
		return $where;
	}
	
	private function user_search($search_data = array()){
		$where = "";
		$sort = "";
		
		if(is_array($search_data) && sizeof($search_data) > 0){
			#Search
			if(isset($search_data['search']) && $search_data['search'] != ""){
				$where .= ($where == "" ? " WHERE " : " AND ") . " (`a`.`username` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%" ).
																	" OR `a`.`email` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `a`.`contact` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `a`.`role` IN (SELECT `id` FROM `role` WHERE `role_name` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%") .")" .
																	" OR `a`.`team` IN (SELECT `id` FROM `team_setting` WHERE `team_name` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%")."))";
			}
			
			#Sorting
			if(isset($search_data['sort-column']) && trim($search_data['sort-column']) != "" && isset($search_data['sort-type']) && trim($search_data['sort-type']) != ""){
				$sort .= ($sort == "" ? " ORDER BY " : ',') . $this->db->protect_identifiers($search_data['sort-column']) . " " . strtoupper($search_data['sort-type']);
			}
		}
		
		return array('where' => $where, 'sort' => $sort);
	}
	
	function user_listing($encrypted_data = ""){
		#Get The Condition
		$condition = $this->user_validate_encrypted_data($encrypted_data);
		
		#Get WHERE
		$where = $condition['where'];
		$where .= ($where == "" ? " WHERE " : " AND ") . " `a`.`email` NOT IN  ('admin')" ; //exclude admin in listing
		
		#Get SORT
		$order_by = $condition['sort'] == "" ? " ORDER BY `a`.`id` DESC " : $condition['sort'];
		
		$this->page["base_url"] = base_url() . "setup/user/".$this->uri->segment(3);
		$this->page["per_page"] = PER_PAGE;
		$this->page["uri_segment"] = 4;
		
		$start_form  = $this->uri->segment(4);
		$limit = " LIMIT " . $this->page["per_page"];
		if(is_numeric($start_form) && $start_form > 1){
			$limit = " LIMIT " . $start_form . ", " . $this->page["per_page"];
		}
		
		$sql_count = "SELECT COUNT(`a`.`id`) AS total FROM `user` AS `a`" . $where;
		$this->page["total_rows"] = $this->db->query($sql_count)->row()->total;
		$this->pagination->initialize($this->page);

		$data['query_user'] = $this->db->query("SELECT `a`.*, `b`.`team_name`, `c`.`role_name` FROM `user` AS `a` LEFT JOIN `team_setting` AS `b` ON `a`.`team` = `b`.`id` LEFT JOIN `role` AS `c` ON `a`.`role` = `c`.`id` " . $where . " " . $order_by . " " . $limit);
		$data['total_row'] = $this->page["total_rows"];
		$data['starting_row'] = $data['query_user']->num_rows() > 0 ? $start_form + 1 : 0;
		$data['stoping_row'] = $start_form + $data['query_user']->num_rows();
		$data['search_data'] = unserialize(base64url_decode($encrypted_data));
		$data['encrypted_data'] = $encrypted_data;
		return $data;
	}
	#user listing end
	
	public function add_user($data = array()){
		$user = array(
			'username' => isset($data['username']) && $data['username'] != '' ? trim($data['username']) : '',
			'password' => isset($data['password']) && $data['password'] != '' ? md5(trim($data['password'])) : '',
			'email' => isset($data['email']) && $data['email'] != '' ? trim($data['email']) : '',
			'contact' => isset($data['contact']) && $data['contact'] != '' ? trim($data['contact']) : '',
			'team' => isset($data['team']) && $data['team'] != '' ? trim($data['team']) : '',
			'role' => isset($data['role']) && $data['role'] != '' ? trim($data['role']) : '',
			'active' => isset($data['active']) && $data['active'] != '' ? trim($data['active']) : 0,
			'create_by' => $this->session->userdata('email'),
			'create_date' => date('Y-m-d H:i:s'),
			'update_by' => $this->session->userdata('email'),
			'update_date' => date('Y-m-d H:i:s'),
		);
		
		if(isset($user['username']) && $user['username'] == ''){
			set_msg('Please insert username', 'danger');
			return false;
		}
		
		if(isset($user['password']) && $user['password'] == ''){
			set_msg('Please insert password', 'danger');
			return false;
		}
		
		if(isset($user['email']) && $user['email'] == ''){
			set_msg('Please insert email', 'danger');
			return false;
		}
		else if(!filter_var($user['email'], FILTER_VALIDATE_EMAIL)){
			set_msg('incorrect email format', 'danger');
			return false;
		}
		
		$sql_check = "SELECT * FROM `user` WHERE `email` = " .$this->db->escape($user['email']). " LIMIT 1";
		$query_check = $this->db->query($sql_check);
		
		if($query_check->num_rows() > 0 ){
			set_msg('Email already exist', 'danger');
			return false;
		}
			
		$this->db->insert('user', $user);
		$user_id = $this->db->insert_id();
		
		if(isset($user['team']) && $user['team'] != ''){
			$this->db->query("INSERT INTO `team_member` (`team_id`,`team_member`) VALUES ('".$user['team']."', '".$user_id."')");
		}
		
		set_msg('You have successfull add new user', 'success');
		return true;
	}
	
	public function user_detail($id = ""){
		if($id > 0){
			$sql = "SELECT * FROM `user` WHERE `id` = " .$this->db->escape($id). " LIMIT 1";
			$query = $this->db->query($sql);
			
			return $query;
		}
	}
	
	public function update_user($id="", $data=array()){
		$user = array(
			'username' => isset($data['username']) && $data['username'] != '' ? trim($data['username']) : '',
			'email' => isset($data['email']) && $data['email'] != '' ? trim($data['email']) : '',
			'contact' => isset($data['contact']) && $data['contact'] != '' ? trim($data['contact']) : '',
			'team' => isset($data['team']) && $data['team'] != '' ? trim($data['team']) : '',
			'role' => isset($data['role']) && $data['role'] != '' ? trim($data['role']) : '',
			'active' => isset($data['active']) && $data['active'] != '' ? trim($data['active']) : 0,
			'update_by' => $this->session->userdata('email'),
			'update_date' => date('Y-m-d H:i:s'),
		);
		
		if(isset($user['username']) && $user['username'] == ''){
			set_msg('Please insert username', 'danger');
			return false;
		}
		
		if(isset($user['password']) && $user['password'] == ''){
			set_msg('Please insert password', 'danger');
			return false;
		}
		
		if(isset($user['email']) && $user['email'] == ''){
			set_msg('Please insert email', 'danger');
			return false;
		}
		else if(!filter_var($user['email'], FILTER_VALIDATE_EMAIL)){
			set_msg('incorrect email format', 'danger');
			return false;
		}
		
		$check_email = $this->db->query("SELECT `email` FROM `user` WHERE `id` = " .$this->db->escape($id));
		if($check_email->num_rows() > 0){
			$email_row = $check_email->row();	
			if($email_row->email != $user['email']){
				$email_exist = $this->db->query("SELECT `email` FROM `user` WHERE `email` = " .$this->db->escape($user['email']));
				if($email_exist->num_rows() > 0){
					set_msg('Email Already Exist', 'danger');
					return false;
				}
			}
		}
		
		$this->db->where('id', $id);
		$this->db->update('user', $user);
		
		#update in tbl team_member
		$sql_team_member = "SELECT * FROM `team_member` WHERE `team_member` = " .$this->db->escape($id);
		$query_team_member = $this->db->query($sql_team_member);
		if($query_team_member->num_rows() > 0){
			$this->db->query("UPDATE `team_member` SET `team_id` = " .$this->db->escape($user['team']). " WHERE `team_member` = " .$this->db->escape($id));
		}
		else{
			$this->db->query("INSERT INTO `team_member` (`team_id`,`team_member`) VALUES ('".$user['team']."', '".$id."')");
		}
		
		set_msg('Successfull', 'success');
		return true;
	}
	
	#team listing start
	private function team_validate_encrypted_data($encrypted_data = ""){
		$where = "";
		$search_data = array();
		if($encrypted_data == ""){
			$encrypted_data = base64url_encode(serialize(array()));
			redirect(base_url().'setup/team/'.$encrypted_data);
		}
		else{
			$search_data = unserialize(base64url_decode($encrypted_data));
			if(is_array($search_data)){
				$where = $this->team_search($search_data);
			}
			else{
				$encrypted_data = base64url_encode(serialize(array()));
				redirect(base_url().'setup/team/'.$encrypted_data);
			}
		}
		
		return $where;
	}
	
	private function team_search($search_data = array()){
		$where = "";
		$sort = "";
		
		if(is_array($search_data) && sizeof($search_data) > 0){
			#Search
			if(isset($search_data['search']) && $search_data['search'] != ""){
				$where .= ($where == "" ? " WHERE " : " AND ") . " (`a`.`team` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%" ).
																	" OR `a`.`team_leader` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `a`.`email` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").")";
			}
			
			#Sorting
			if(isset($search_data['sort-column']) && trim($search_data['sort-column']) != "" && isset($search_data['sort-type']) && trim($search_data['sort-type']) != ""){
				$sort .= ($sort == "" ? " ORDER BY " : ',') . $this->db->protect_identifiers($search_data['sort-column']) . " " . strtoupper($search_data['sort-type']);
			}
		}
		
		return array('where' => $where, 'sort' => $sort);
	}
	
	function team_listing($encrypted_data = ""){
		#Get The Condition
		$condition = $this->team_validate_encrypted_data($encrypted_data);
		
		#Get WHERE
		$where = $condition['where'];
		// $where .= ($where == "" ? " WHERE " : " AND ") . " `company_id` = " . $this->db->escape(1) . " AND `request_status` = 'pending' ";
		
		#Get SORT
		$order_by = $condition['sort'] == "" ? " ORDER BY `a`.`id` DESC " : $condition['sort'];
		
		$this->page["base_url"] = base_url() . "setup/team/".$this->uri->segment(3);
		$this->page["per_page"] = PER_PAGE;
		$this->page["uri_segment"] = 4;
		
		$start_form  = $this->uri->segment(4);
		$limit = " LIMIT " . $this->page["per_page"];
		if(is_numeric($start_form) && $start_form > 1){
			$limit = " LIMIT " . $start_form . ", " . $this->page["per_page"];
		}
		
		$sql_count = "SELECT COUNT(`a`.`id`) AS total FROM `team_setting` AS `a` LEFT JOIN `user` AS `b` ON `a`.`team_leader` = `b`.`id`" . $where;
		$this->page["total_rows"] = $this->db->query($sql_count)->row()->total;
		$this->pagination->initialize($this->page);

		$data['query_team'] = $this->db->query("SELECT `a`.*, `b`.`username` FROM `team_setting` AS `a` LEFT JOIN `user` AS `b` ON `a`.`team_leader` = `b`.`id` " . $where . " " . $order_by . " " . $limit);
		$data['total_row'] = $this->page["total_rows"];
		$data['starting_row'] = $data['query_team']->num_rows() > 0 ? $start_form + 1 : 0;
		$data['stoping_row'] = $start_form + $data['query_team']->num_rows();
		$data['search_data'] = unserialize(base64url_decode($encrypted_data));
		$data['encrypted_data'] = $encrypted_data;
		return $data;
	}
	#team listing end
	
	public function get_team_member($return_type = "query", $condition = array()){
		$where = "";
		
		#If condition is array, loop to extract where condition into string
		if(is_array($condition)){
			if(sizeof($condition) > 0){
				foreach($condition as $column_name => $column_value){
					$where .= ($where == "" ? " WHERE " : " AND ") . $this->db->protect_identifiers($column_name) . " = " . $this->db->escape($column_value);
				}
			}
		}
		else{
			#Condition in string contains where condition
			$where = $condition;
		}
		
		$query = $this->db->query("SELECT `a`.*, `b`.`username` FROM `team_member` AS `a` LEFT JOIN `user` AS `b` ON `a`.`team_member` = `b`.`id` $where GROUP BY `a`.`team_member`");
		if($return_type == "row"){
			if($query->num_rows() > 0){
				return $query->row();
			}
			
			return new stdClass();
		}
		
		return $query;
	}
	
	public function add_team($data=array()){
		if(is_array($data) && sizeof($data) > 0){
			$team_member = isset($data['team_member']) && is_array($data['team_member']) && sizeof($data['team_member']) > 0 ? $data['team_member'] : '';
			
			$team = array(
				'team_name' => isset($data['team_name']) && $data['team_name'] != '' ? trim($data['team_name']) : '',
				'team_leader' => isset($data['team_leader']) && $data['team_leader'] != '' ? trim($data['team_leader']) : '',
				'team_email' => isset($data['team_email']) && $data['team_email'] != '' ? trim($data['team_email']) : '',
				'active' => isset($data['active']) && $data['active'] != '' ? trim($data['active']) : 0,
				'create_by' => $this->session->userdata('email'), 
				'create_date' => date('Y-m-d H:i:s'),
				'update_by' => $this->session->userdata('email'), 
				'update_date' => date('Y-m-d H:i:s')
			);
			
			if(isset($team['team_name']) && $team['team_name'] == ""){
				set_msg('Please insert team name', 'danger');
				return false;
			}
			
			if(isset($team['team_email']) && $team['team_email'] == ''){
				set_msg('Please insert email', 'danger');
				return false;
			}
			else if(!filter_var($team['team_email'], FILTER_VALIDATE_EMAIL)){
				set_msg('incorrect email format', 'danger');
				return false;
			}
			
			$this->db->insert('team_setting', $team);
			$team_id = $this->db->insert_id();
			
			if(is_array($team_member) && sizeof($team_member) > 0){
				foreach($team_member as $key => $row_member){
					if($team_member[$key] != ''){
						$team_members = array(
							'team_id' => isset($team_id) && $team_id > 0 ? $team_id : '',
							'team_member' => isset($row_member) && $row_member != '' ? $row_member : '',
							'create_by' => $this->session->userdata('email'), 
							'create_date' => date('Y-m-d H:i:s'),
							'update_by' => $this->session->userdata('email'), 
							'update_date' => date('Y-m-d H:i:s')
						);
						
						$this->db->insert('team_member', $team_members);
					}
				}
			}
			
			set_msg('Successfull', 'success');
			return true;
		}
	}
	
	public function update_team($id="", $data=array()){
		if(is_array($data) && sizeof($data) > 0){
			$team_member = isset($data['team_member']) && is_array($data['team_member']) && sizeof($data['team_member']) > 0 ? $data['team_member'] : '';
			
			$team = array(
				'team_name' => isset($data['team_name']) && $data['team_name'] != '' ? trim($data['team_name']) : '',
				'team_leader' => isset($data['team_leader']) && $data['team_leader'] != '' ? trim($data['team_leader']) : '',
				'team_email' => isset($data['team_email']) && $data['team_email'] != '' ? trim($data['team_email']) : '',
				'active' => isset($data['active']) && $data['active'] != '' ? trim($data['active']) : 0,
				'update_by' => $this->session->userdata('email'), 
				'update_date' => date('Y-m-d H:i:s')
			);
			
			if(isset($team['team_name']) && $team['team_name'] == ""){
				set_msg('Please insert team name', 'danger');
				return false;
			}
			
			if(isset($team['team_email']) && $team['team_email'] == ''){
				set_msg('Please insert email', 'danger');
				return false;
			}
			else if(!filter_var($team['team_email'], FILTER_VALIDATE_EMAIL)){
				set_msg('incorrect email format', 'danger');
				return false;
			}
			
			$this->db->where('id', $id);
			$this->db->update('team_setting', $team);
			
			$sql_member = "SELECT * FROM `team_member` WHERE `team_id` = " .$this->db->escape($id);
			$query_member = $this->db->query($sql_member);
			
			if($query_member->num_rows() > 0){
				#delete current team member 
				$this->db->query("DELETE FROM `team_member` WHERE `team_id` = " .$this->db->escape($id));
			}
			
			#insert updated team member
			if(is_array($team_member) && sizeof($team_member) > 0){
				foreach($team_member as $key => $row_member){
					if($team_member[$key] != ''){
						//update current user's team tu empty
						$query_check_team = $this->db->query("SELECT * FROM `user` WHERE `team` = " .$this->db->escape($id));
						if($query_check_team->num_rows() > 0){
							$this->db->query("UPDATE `user` SET `team` = '' WHERE `team` = " .$this->db->escape($id));
						}
						
						$this->db->query("UPDATE `user` SET `team` = " .$this->db->escape($id). " WHERE `id` = " .$this->db->escape($row_member));
						
						$team_members = array(
							'team_id' => isset($id) && $id > 0 ? $id : '',
							'team_member' => isset($row_member) && $row_member != '' ? $row_member : '',
							'create_by' => $this->session->userdata('email'), 
							'create_date' => date('Y-m-d H:i:s'),
							'update_by' => $this->session->userdata('email'), 
							'update_date' => date('Y-m-d H:i:s')
						);
						
						$this->db->insert('team_member', $team_members);
					}
				} 
			}
			
			set_msg('Successfull','success');
			return true;
			
		}
	}
	
	public function team_detail($id = ''){
		if(isset($id) && $id > 0){
			$sql = "SELECT * FROM `team_setting` WHERE `id` = " .$this->db->escape($id);
			$query = $this->db->query($sql);
				
			return $query;
		}
	}
	
	public function active_user(){
		$ignore = array('1'); //add user id in array if want to ignore user 
		
		$condition = array(
			'active' => 1
		);
		
		$this->db->where($condition);
		$this->db->where_not_in('id', $ignore);
		
		$query = $this->db->get('user');

		return $query;
	}
	
	public function active_team(){
		$sql = "SELECT * FROM `team_setting` WHERE `active` = '1'";
		$query = $this->db->query($sql);
		
		return $query;
	}
	
	public function active_role(){
		$sql = "SELECT * FROM `role` WHERE `active` = '1'";
		$query = $this->db->query($sql);
		
		return $query;
	}
	
	#role listing start
	private function role_validate_encrypted_data($encrypted_data = ""){
		$where = "";
		$search_data = array();
		if($encrypted_data == ""){
			$encrypted_data = base64url_encode(serialize(array()));
			redirect(base_url().'setup/role/'.$encrypted_data);
		}
		else{
			$search_data = unserialize(base64url_decode($encrypted_data));
			if(is_array($search_data)){
				$where = $this->role_search($search_data);
			}
			else{
				$encrypted_data = base64url_encode(serialize(array()));
				redirect(base_url().'setup/role/'.$encrypted_data);
			}
		}
		
		return $where;
	}
	
	private function role_search($search_data = array()){
		$where = "";
		$sort = "";
		
		if(is_array($search_data) && sizeof($search_data) > 0){
			#Search
			if(isset($search_data['search']) && $search_data['search'] != ""){
				$where .= ($where == "" ? " WHERE " : " AND ") . " (`role_name` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%" ).
																	" OR `active` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").")";
			}
			
			#Sorting
			if(isset($search_data['sort-column']) && trim($search_data['sort-column']) != "" && isset($search_data['sort-type']) && trim($search_data['sort-type']) != ""){
				$sort .= ($sort == "" ? " ORDER BY " : ',') . $this->db->protect_identifiers($search_data['sort-column']) . " " . strtoupper($search_data['sort-type']);
			}
		}
		
		return array('where' => $where, 'sort' => $sort);
	}
	
	function role_listing($encrypted_data = ""){
		#Get The Condition
		$condition = $this->role_validate_encrypted_data($encrypted_data);
		
		#Get WHERE
		$where = $condition['where'];
		// $where .= ($where == "" ? " WHERE " : " AND ") . " `company_id` = " . $this->db->escape(1) . " AND `request_status` = 'pending' ";
		
		#Get SORT
		$order_by = $condition['sort'] == "" ? " ORDER BY `id` DESC " : $condition['sort'];
		
		$this->page["base_url"] = base_url() . "setup/role/".$this->uri->segment(3);
		$this->page["per_page"] = PER_PAGE;
		$this->page["uri_segment"] = 4;
		
		$start_form  = $this->uri->segment(4);
		$limit = " LIMIT " . $this->page["per_page"];
		if(is_numeric($start_form) && $start_form > 1){
			$limit = " LIMIT " . $start_form . ", " . $this->page["per_page"];
		}
		
		$sql_count = "SELECT COUNT(`id`) AS total FROM `role`" . $where;
		$this->page["total_rows"] = $this->db->query($sql_count)->row()->total;
		$this->pagination->initialize($this->page);

		$data['query_role'] = $this->db->query("SELECT * FROM `role` " . $where . " " . $order_by . " " . $limit);
		$data['total_row'] = $this->page["total_rows"];
		$data['starting_row'] = $data['query_role']->num_rows() > 0 ? $start_form + 1 : 0;
		$data['stoping_row'] = $start_form + $data['query_role']->num_rows();
		$data['search_data'] = unserialize(base64url_decode($encrypted_data));
		$data['encrypted_data'] = $encrypted_data;
		return $data;
	}
	#role listing end
	
	public function get_page_access(){
		$sql = "SELECT * FROM `role_page_access`";
		$query = $this->db->query($sql);
		
		return $query;
	}
	
	public function add_role($data=array()){
		$add_privilege = isset($data['add']) && is_array($data['add']) && sizeof($data['add']) > 0 ? $data['add'] : '';
		$view_privilege = isset($data['view']) && is_array($data['view']) && sizeof($data['view']) > 0 ? $data['view'] : '';
		$edit_privilege = isset($data['edit']) && is_array($data['edit']) && sizeof($data['edit']) > 0 ? $data['edit'] : '';
		$delete_privilege = isset($data['delete']) && is_array($data['delete']) && sizeof($data['delete']) > 0 ? $data['delete'] : '';
		
		$role = array(
			'role_name' => isset($data['role_name']) && $data['role_name'] != '' ? ucwords(trim($data['role_name'])) : '',
			'active' => isset($data['active']) && $data['active'] != '' ? trim($data['active']) : 0,
			'create_by' => $this->session->userdata('email'),
			'create_date' => date('Y-m-d H:i:s'),
			'update_by' => $this->session->userdata('email'),
			'update_date' => date('Y-m-d H:i:s')
		);
		
		if(isset($role['role_name']) && $role['role_name'] == ''){
			set_msg('Please insert role name', 'dangers');
			return false;
		}
		
		$this->db->insert('role', $role);
		$role_id = $this->db->insert_id();
		
		$role_privilege = array(
			'role_id' => isset($role_id) && $role_id != '' ? $role_id : '',
			'create_by' => $this->session->userdata('email'),
			'create_date' => date('Y-m-d H:i:s'),
			'update_by' => $this->session->userdata('email'),
			'update_date' => date('Y-m-d H:i:s')
		);
		
		#insert for privilege
		$query_page_access = $this->db->query("SELECT * FROM `role_page_access`");
		if($query_page_access->num_rows() > 0){
			foreach($query_page_access->result() as $row_access){
				$role_privilege['page_access_id'] = isset($row_access->id) && $row_access->id != '' ? $row_access->id : '';
				$role_privilege['view_only'] = isset($view_privilege[$row_access->id]) && $view_privilege[$row_access->id] != '' ? $view_privilege[$row_access->id] : 0;
				$role_privilege['add_only'] = isset($add_privilege[$row_access->id]) && $add_privilege[$row_access->id] != '' ? $add_privilege[$row_access->id] : 0;
				$role_privilege['edit_only'] = isset($edit_privilege[$row_access->id]) && $edit_privilege[$row_access->id] != '' ? $edit_privilege[$row_access->id] : 0;
				$role_privilege['delete_only'] = isset($delete_privilege[$row_access->id]) && $delete_privilege[$row_access->id] != '' ? $delete_privilege[$row_access->id] : 0;
				
				$this->db->insert('role_privilege', $role_privilege);
			}
		}
		
		set_msg('Successfull', 'success');
		return true;
	}
	
	public function get_role_setting($id=''){
		$sql = "SELECT * FROM `role` WHERE `id` = " .$this->db->escape($id);
		$query = $this->db->query($sql);
		
		return $query; 
	}
	
	public function get_privilege($return_type = "query", $condition = array()){
		$where = "";
		
		#If condition is array, loop to extract where condition into string
		if(is_array($condition)){
			if(sizeof($condition) > 0){
				foreach($condition as $column_name => $column_value){
					$where .= ($where == "" ? " WHERE " : " AND ") . $this->db->protect_identifiers($column_name) . " = " . $this->db->escape($column_value);
				}
			}
		}
		else{
			#Condition in string contains where condition
			$where = $condition;
		}
		
		$query = $this->db->query("SELECT * FROM `role_privilege` $where");
		if($return_type == "row"){
			if($query->num_rows() > 0){
				return $query->row();
			}
			
			return new stdClass();
		}
		
		return $query;
	}
	
	public function update_role($id='', $data=array()){
		$add_privilege = isset($data['add']) && is_array($data['add']) && sizeof($data['add']) > 0 ? $data['add'] : '';
		$view_privilege = isset($data['view']) && is_array($data['view']) && sizeof($data['view']) > 0 ? $data['view'] : '';
		$edit_privilege = isset($data['edit']) && is_array($data['edit']) && sizeof($data['edit']) > 0 ? $data['edit'] : '';
		$delete_privilege = isset($data['delete']) && is_array($data['delete']) && sizeof($data['delete']) > 0 ? $data['delete'] : '';
		
		$role = array(
			'role_name' => isset($data['role_name']) && $data['role_name'] != '' ? ucwords(trim($data['role_name'])) : '',
			'active' => isset($data['active']) && $data['active'] != '' ? trim($data['active']) : 0,
			'update_by' => $this->session->userdata('email'),
			'update_date' => date('Y-m-d H:i:s')
		);
		
		if(isset($role['role_name']) && $role['role_name'] == ''){
			set_msg('Please insert role name', 'danger');
			return false;
		}
		
		$this->db->where('id', $id);
		$this->db->update('role', $role);
		
		$role_privilege = array(
			'create_by' => $this->session->userdata('email'),
			'create_date' => date('Y-m-d H:i:s'),
			'update_by' => $this->session->userdata('email'),
			'update_date' => date('Y-m-d H:i:s')
		);
		
		#insert for privilege
		$query_page_access = $this->db->query("SELECT * FROM `role_page_access`");
		if($query_page_access->num_rows() > 0){
			foreach($query_page_access->result() as $row_access){
				$role_privilege['view_only'] = isset($view_privilege[$row_access->id]) && $view_privilege[$row_access->id] != '' ? $view_privilege[$row_access->id] : 0;
				$role_privilege['add_only'] = isset($add_privilege[$row_access->id]) && $add_privilege[$row_access->id] != '' ? $add_privilege[$row_access->id] : 0;
				$role_privilege['edit_only'] = isset($edit_privilege[$row_access->id]) && $edit_privilege[$row_access->id] != '' ? $edit_privilege[$row_access->id] : 0;
				$role_privilege['delete_only'] = isset($delete_privilege[$row_access->id]) && $delete_privilege[$row_access->id] != '' ? $delete_privilege[$row_access->id] : 0;
				
				$query_check = $this->db->query("SELECT * FROM `role_privilege` WHERE `role_id` = " .$id. " AND `page_access_id` = ". $row_access->id);
				if($query_check->num_rows() > 0){
					$this->db->where(array('role_id' => $id, 'page_access_id' => $row_access->id));
					$this->db->update('role_privilege', $role_privilege);
				}
				else{
					$role_privilege['role_id'] = isset($id) && $id != '' ? $id : '';
					$role_privilege['page_access_id'] = isset($row_access->id) && $row_access->id != '' ? $row_access->id : '';
					
					$this->db->insert('role_privilege', $role_privilege);
				}
			}
		}
		
		set_msg('Successfull', 'success');
		return true;
	}
	
	#subcont listing start
	private function subc_validate_encrypted_data($encrypted_data = ""){
		$where = "";
		$search_data = array();
		if($encrypted_data == ""){
			$encrypted_data = base64url_encode(serialize(array()));
			redirect(base_url().'setup/subcont/'.$encrypted_data);
		}
		else{
			$search_data = unserialize(base64url_decode($encrypted_data));
			if(is_array($search_data)){
				$where = $this->subc_search($search_data);
			}
			else{
				$encrypted_data = base64url_encode(serialize(array()));
				redirect(base_url().'setup/subcont/'.$encrypted_data);
			}
		}
		
		return $where;
	}
	
	private function subc_search($search_data = array()){
		$where = "";
		$sort = "";
		
		if(is_array($search_data) && sizeof($search_data) > 0){
			#Search
			if(isset($search_data['search']) && $search_data['search'] != ""){
				$where .= ($where == "" ? " WHERE " : " AND ") . " (`sub_code` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%" ).
																	" OR `sub_name` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `sub_type` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `company_no` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `gst_reg_no` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `address` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `pic` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `phone` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `email` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `cert_value` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `limit_retention` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `payment_term` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").")";
			}
			
			#Sorting
			if(isset($search_data['sort-column']) && trim($search_data['sort-column']) != "" && isset($search_data['sort-type']) && trim($search_data['sort-type']) != ""){
				$sort .= ($sort == "" ? " ORDER BY " : ',') . $this->db->protect_identifiers($search_data['sort-column']) . " " . strtoupper($search_data['sort-type']);
			}
		}
		
		return array('where' => $where, 'sort' => $sort);
	}
	
	function subc_listing($encrypted_data = ""){
		#Get The Condition
		$condition = $this->subc_validate_encrypted_data($encrypted_data);
		
		#Get WHERE
		$where = $condition['where'];
		// $where .= ($where == "" ? " WHERE " : " AND ") . " `company_id` = " . $this->db->escape(1) . " AND `request_status` = 'pending' ";
		
		#Get SORT
		$order_by = $condition['sort'] == "" ? " ORDER BY `id` DESC " : $condition['sort'];
		
		$this->page["base_url"] = base_url() . "setup/subcont/".$this->uri->segment(3);
		$this->page["per_page"] = PER_PAGE;
		$this->page["uri_segment"] = 4;
		
		$start_form  = $this->uri->segment(4);
		$limit = " LIMIT " . $this->page["per_page"];
		if(is_numeric($start_form) && $start_form > 1){
			$limit = " LIMIT " . $start_form . ", " . $this->page["per_page"];
		}
		
		$sql_count = "SELECT COUNT(`id`) AS total FROM `subcont`" . $where;
		$this->page["total_rows"] = $this->db->query($sql_count)->row()->total;
		$this->pagination->initialize($this->page);

		$data['query_subcont'] = $this->db->query("SELECT * FROM `subcont` " . $where . " " . $order_by . " " . $limit);
		$data['total_row'] = $this->page["total_rows"];
		$data['starting_row'] = $data['query_subcont']->num_rows() > 0 ? $start_form + 1 : 0;
		$data['stoping_row'] = $start_form + $data['query_subcont']->num_rows();
		$data['search_data'] = unserialize(base64url_decode($encrypted_data));
		$data['encrypted_data'] = $encrypted_data;
		return $data;
	}
	#subcont listing end
	
	public function add_subcont($data=array()){
		if(is_array($data) && sizeof($data) > 0 ){
			
			$subcont = array(
				'sub_code' => isset($data['sub_code']) && $data['sub_code'] != '' ? strtoupper(trim($data['sub_code'])) : '',
				'sub_name' => isset($data['sub_name']) && $data['sub_name'] != '' ? trim($data['sub_name']) : '',
				'sub_type' => isset($data['sub_type']) && $data['sub_type'] != '' ? trim($data['sub_type']) : '',
				'company_no' => isset($data['company_no']) && $data['company_no'] != '' ? trim($data['company_no']) : '',
				'gst_reg_no' => isset($data['gst_reg_no']) && $data['gst_reg_no'] != '' ? trim($data['gst_reg_no']) : '',
				'address' => isset($data['address']) && $data['address'] != '' ? ucwords(trim($data['address'])) : '',
				'pic' => isset($data['pic']) && $data['pic'] != '' ? trim($data['pic']) : '',
				'phone' => isset($data['phone']) && $data['phone'] != '' ? trim($data['phone']) : '',
				'email' => isset($data['email']) && $data['email'] != '' ? trim($data['email']) : '',
				'cert_value' => isset($data['cert_value']) && $data['cert_value'] != '' ? trim($data['cert_value']) : '',
				'limit_retention' => isset($data['limit_retention']) && $data['limit_retention'] != '' ? ucwords(trim($data['limit_retention'])) : '',
				'payment_term' => isset($data['payment_term']) && $data['payment_term'] != '' ? trim($data['payment_term']) : '',
				'active' => isset($data['active']) && $data['active'] != '' ? $data['active'] : '0',
				'create_by' => $this->session->userdata('email'),
				'create_date' => date('Y-m-d H:i:s'),
				'update_by' => $this->session->userdata('email'),
				'update_date' => date('Y-m-d H:i:s')
			);
			
			if(isset($subcont['sub_code']) && $subcont['sub_code'] == ''){
				set_msg('Please insert subcont code', 'danger');
				return false;
			}
			
			if(isset($subcont['sub_name']) && $subcont['sub_name'] == ''){
				set_msg('Please insert subcont name', 'danger');
				return false;
			}
			
			if(isset($subcont['company_no']) && $subcont['company_no'] == ''){
				set_msg('Please insert company no', 'danger');
				return false;
			}
			
			if(isset($subcont['gst_reg_no']) && $subcont['gst_reg_no'] == ''){
				set_msg('Please insert GST registration no', 'danger');
				return false;
			}
			
			if(isset($subcont['address']) && $subcont['address'] == ''){
				set_msg('Please insert address', 'danger');
				return false;
			}
			
			if(isset($subcont['pic']) && $subcont['pic'] == ''){
				set_msg('Please insert PIC', 'danger');
				return false;
			}
			
			$this->db->insert('subcont', $subcont);
			set_msg('Successfull','success');
			return true;
		}
		
	}
	
	public function get_subcont_detailed($id){
		$sql = "SELECT * FROM `subcont` WHERE `id` = " .$this->db->escape($id);
		$query = $this->db->query($sql);
		
		return $query;
	}
	
	public function update_subcont($id='', $data=array()){
		if(is_array($data) && sizeof($data) > 0 ){
			
			$subcont = array(
				'sub_code' => isset($data['sub_code']) && $data['sub_code'] != '' ? strtoupper(trim($data['sub_code'])) : '',
				'sub_name' => isset($data['sub_name']) && $data['sub_name'] != '' ? trim($data['sub_name']) : '',
				'sub_type' => isset($data['sub_type']) && $data['sub_type'] != '' ? trim($data['sub_type']) : '',
				'company_no' => isset($data['company_no']) && $data['company_no'] != '' ? trim($data['company_no']) : '',
				'gst_reg_no' => isset($data['gst_reg_no']) && $data['gst_reg_no'] != '' ? trim($data['gst_reg_no']) : '',
				'address' => isset($data['address']) && $data['address'] != '' ? ucwords(trim($data['address'])) : '',
				'pic' => isset($data['pic']) && $data['pic'] != '' ? trim($data['pic']) : '',
				'phone' => isset($data['phone']) && $data['phone'] != '' ? trim($data['phone']) : '',
				'email' => isset($data['email']) && $data['email'] != '' ? trim($data['email']) : '',
				'cert_value' => isset($data['cert_value']) && $data['cert_value'] != '' ? trim($data['cert_value']) : '',
				'limit_retention' => isset($data['limit_retention']) && $data['limit_retention'] != '' ? ucwords(trim($data['limit_retention'])) : '',
				'payment_term' => isset($data['payment_term']) && $data['payment_term'] != '' ? trim($data['payment_term']) : '',
				'active' => isset($data['active']) && $data['active'] != '' ? $data['active'] : '0',
				'update_by' => $this->session->userdata('email'),
				'update_date' => date('Y-m-d H:i:s')
			);
			
			if(isset($subcont['sub_code']) && $subcont['sub_code'] == ''){
				set_msg('Please insert subcont code', 'danger');
				return false;
			}
			
			if(isset($subcont['sub_name']) && $subcont['sub_name'] == ''){
				set_msg('Please insert subcont name', 'danger');
				return false;
			}
			
			if(isset($subcont['company_no']) && $subcont['company_no'] == ''){
				set_msg('Please insert company no', 'danger');
				return false;
			}
			
			if(isset($subcont['gst_reg_no']) && $subcont['gst_reg_no'] == ''){
				set_msg('Please insert GST registration no', 'danger');
				return false;
			}
			
			if(isset($subcont['address']) && $subcont['address'] == ''){
				set_msg('Please insert address', 'danger');
				return false;
			}
			
			if(isset($subcont['pic']) && $subcont['pic'] == ''){
				set_msg('Please insert PIC', 'danger');
				return false;
			}
			
			$this->db->where('id',$id);
			$this->db->update('subcont', $subcont);
			set_msg('Successfull','success');
			return true;
		}
	}
	
	public function get_project_team($id = ''){
		if($id > 0){
			
			$data = array();
			
			$sql = "SELECT * FROM `team_setting` WHERE `team_leader` = " .$this->db->escape($id). " LIMIT 1";
			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				$team_id = $query->row()->id;
				
				$sql_member = "SELECT `a`.*,`b`.`username` 
								FROM `team_member` AS `a` 
								INNER JOIN `user` AS `b` ON `a`.`team_member` = `b`.`id` 
								WHERE `a`.`team_id` = " .$this->db->escape($team_id);
								
				$query_member = $this->db->query($sql_member);
				
				$data = $query_member->result_array();
				return $data;
				
			}
			
			return $data;
			
		}
	}
	
	public function add_project($data=array()){
		if(is_array($data) && sizeof($data)){
			$management_position = isset($data['management_position']) && is_array($data['management_position']) && sizeof($data['management_position']) > 0 ? $data['management_position'] : ''; 
			$management_name = isset($data['management_name']) && is_array($data['management_name']) && sizeof($data['management_name']) > 0 ? $data['management_name'] : ''; 
			$completion_block = isset($data['completion_block']) && is_array($data['completion_block']) && sizeof($data['completion_block']) > 0 ? $data['completion_block'] : ''; 
			$completion_commencement = isset($data['completion_commencement']) && is_array($data['completion_commencement']) && sizeof($data['completion_commencement']) > 0 ? $data['completion_commencement'] : ''; 
			$completion_date = isset($data['completion_date']) && is_array($data['completion_date']) && sizeof($data['completion_date']) > 0 ? $data['completion_date'] : ''; 
			$completion_latest_eot = isset($data['completion_latest_eot']) && is_array($data['completion_latest_eot']) && sizeof($data['completion_latest_eot']) > 0 ? $data['completion_latest_eot'] : ''; 
			$completion_level_quantity = isset($data['completion_level_quantity']) && is_array($data['completion_level_quantity']) && sizeof($data['completion_level_quantity']) > 0 ? $data['completion_level_quantity'] : ''; 
			$completion_unit_quantity = isset($data['completion_unit_quantity']) && is_array($data['completion_unit_quantity']) && sizeof($data['completion_unit_quantity']) > 0 ? $data['completion_unit_quantity'] : ''; 
			$planed_block = isset($data['planed_block']) && is_array($data['planed_block']) && sizeof($data['planed_block']) > 0 ? $data['planed_block'] : ''; 
			$planned_fcf_submission = isset($data['planned_fcf_submission']) && is_array($data['planned_fcf_submission']) && sizeof($data['planned_fcf_submission']) > 0 ? $data['planned_fcf_submission'] : ''; 
			$planned_production_order = isset($data['planned_production_order']) && is_array($data['planned_production_order']) && sizeof($data['planned_production_order']) > 0 ? $data['planned_production_order'] : ''; 
			$planned_team_issued = isset($data['planned_team_issued']) && is_array($data['planned_team_issued']) && sizeof($data['planned_team_issued']) > 0 ? $data['planned_team_issued'] : ''; 
			$planned_installation_start = isset($data['planned_installation_start']) && is_array($data['planned_installation_start']) && sizeof($data['planned_installation_start']) > 0 ? $data['planned_installation_start'] : ''; 
			$planned_completion_date = isset($data['planned_completion_date']) && is_array($data['planned_completion_date']) && sizeof($data['planned_completion_date']) > 0 ? $data['planned_completion_date'] : ''; 
			$planned_dlp_end = isset($data['planned_dlp_end']) && is_array($data['planned_dlp_end']) && sizeof($data['planned_dlp_end']) > 0 ? $data['planned_dlp_end'] : ''; 
			
			$project_setup = array(
				'project_name' => isset($data['project_name']) && $data['project_name'] != '' ? $data['project_name'] : '',
				'project_code' => isset($data['project_code']) && $data['project_code'] != '' ? $data['project_code'] : '',
				'dev_name' => isset($data['dev_name']) && $data['dev_name'] != '' ? $data['dev_name'] : '',
				'dev_contact' => isset($data['dev_contact']) && $data['dev_contact'] != '' ? $data['dev_contact'] : '',
				'pro_company_name' => isset($data['pro_company_name']) && $data['pro_company_name'] != '' ? $data['pro_company_name'] : '',
				'pro_company_contact' => isset($data['pro_company_contact']) && $data['pro_company_contact'] != '' ? $data['pro_company_contact'] : '',
				'main_company_name' => isset($data['main_company_name']) && $data['main_company_name'] != '' ? $data['main_company_name'] : '',
				'main_company_contact' => isset($data['main_company_contact']) && $data['main_company_contact'] != '' ? $data['main_company_contact'] : '',
				'architect_name' => isset($data['architect_name']) && $data['architect_name'] != '' ? $data['architect_name'] : '',
				'architect_contact' => isset($data['architect_contact']) && $data['architect_contact'] != '' ? $data['architect_contact'] : '',
				'designer_name' => isset($data['designer_name']) && $data['designer_name'] != '' ? $data['designer_name'] : '',
				'designer_contact' => isset($data['designer_contact']) && $data['designer_contact'] != '' ? $data['designer_contact'] : '',
				'sales_team' => isset($data['sales_team']) && $data['sales_team'] != '' ? $data['sales_team'] : '',
				'project_unit' => isset($data['project_unit']) && $data['project_unit'] != '' ? $data['project_unit'] : '',
				'loa_date' => isset($data['loa_date']) && $data['loa_date'] != '' ? date('Y-m-d', strtotime($data['loa_date'])) : '',
				'dlp_date' => isset($data['dlp_date']) && $data['dlp_date'] != '' ? date('Y-m-d', strtotime($data['dlp_date']))  : '',
				'lad_amount' => isset($data['lad_amount']) && $data['lad_amount'] != '' ? $data['lad_amount'] : '',
				'target_completion_date' => isset($data['target_completion_date']) && $data['target_completion_date'] != '' ? date('Y-m-d', strtotime($data['target_completion_date'])) : '',
				'work_inprogress' => isset($data['work_inprogress']) && $data['work_inprogress'] != '' ? $data['work_inprogress'] : '',
				'remarks' => isset($data['remarks']) && $data['remarks'] != '' ? $data['remarks'] : '',
				'create_date' => isset($data['create_date']) && $data['create_date'] != '' ? date('Y-m-d', strtotime($data['create_date'])) : '',
				'create_by' => $this->session->userdata('email'),
				'update_date' => date('Y-m-d H:i:s'),
				'update_by' => $this->session->userdata('email'),
			);
			
			$this->db->insert('project_setup', $project_setup);
			$project_id = $this->db->insert_id();
			
			foreach($management_position as $key_position => $val_position){
				$project_site_management = array(
					'project_id' => $project_id,
					'management_position' => isset($val_position) &&  $val_position != '' ? $val_position : '',
					'management_id' => isset($management_name[$key_position]) &&  $management_name[$key_position] != '' ? $management_name[$key_position] : '',
					'create_date' => date('Y-m-d H:i:s'),
					'create_by' => $this->session->userdata('email'),
					'update_date' => date('Y-m-d H:i:s'),
					'update_by' => $this->session->userdata('email'),
				);
				
				$this->db->insert('project_site_management', $project_site_management);
			}
			
			foreach($completion_block as $key_completion => $val_completion){
				$project_completion = array(
					'project_id' => $project_id,
					'block' => isset($val_completion) &&  $val_completion != '' ? $val_completion : '',
					'completion_commencement' => isset($completion_commencement[$key_completion]) &&  $completion_commencement[$key_completion] != '' ? date('Y-m-d', strtotime($completion_commencement[$key_completion])) : '',
					'completion_date' => isset($completion_date[$key_completion]) &&  $completion_date[$key_completion] != '' ? date('Y-m-d', strtotime($completion_date[$key_completion])) : '',
					'completion_latest_eot' => isset($completion_latest_eot[$key_completion]) &&  $completion_latest_eot[$key_completion] != '' ? date('Y-m-d', strtotime($completion_latest_eot[$key_completion]))  : '',
					'completion_level_quantity' => isset($completion_level_quantity[$key_completion]) &&  $completion_level_quantity[$key_completion] != '' ? $completion_level_quantity[$key_completion] : '',
					'completion_unit_quantity' => isset($completion_unit_quantity[$key_completion]) &&  $completion_unit_quantity[$key_completion] != '' ? $completion_unit_quantity[$key_completion] : '',
					'create_date' => date('Y-m-d H:i:s'),
					'create_by' => $this->session->userdata('email'),
					'update_date' => date('Y-m-d H:i:s'),
					'update_by' => $this->session->userdata('email'),
				);
				
				$this->db->insert('project_completion', $project_completion);
			}
			
			foreach($planed_block as $key_planned => $val_planned){
				$project_planned = array(
					'project_id' => $project_id,
					'block' => isset($val_planned) &&  $val_planned != '' ? $val_planned : '',
					'planned_fcf_submission' => isset($planned_fcf_submission[$key_planned]) &&  $planned_fcf_submission[$key_planned] != '' ? date('Y-m-d', strtotime($planned_fcf_submission[$key_planned])) : '',
					'planned_production_order' => isset($planned_production_order[$key_planned]) &&  $planned_production_order[$key_planned] != '' ? date('Y-m-d', strtotime($planned_production_order[$key_planned])) : '',
					'planned_team_issued' => isset($planned_team_issued[$key_planned]) &&  $planned_team_issued[$key_planned] != '' ? date('Y-m-d', strtotime($planned_team_issued[$key_planned])) : '',
					'planned_installation_start' => isset($planned_installation_start[$key_planned]) &&  $planned_installation_start[$key_planned] != '' ? date('Y-m-d', strtotime($planned_installation_start[$key_planned])) : '',
					'planned_completion_date' => isset($planned_completion_date[$key_planned]) &&  $planned_completion_date[$key_planned] != '' ? date('Y-m-d', strtotime($planned_completion_date[$key_planned])) : '',
					'planned_dlp_end' => isset($planned_dlp_end[$key_planned]) &&  $planned_dlp_end[$key_planned] != '' ? date('Y-m-d', strtotime($planned_dlp_end[$key_planned])) : '',
					'create_date' => date('Y-m-d H:i:s'),
					'create_by' => $this->session->userdata('email'),
					'update_date' => date('Y-m-d H:i:s'),
					'update_by' => $this->session->userdata('email'),
				);
				
				$this->db->insert('project_planned', $project_planned);
			}
			
			set_msg('Successfull', 'success');
			return true;
		}
	}
	
	public function update_project($id='', $data=array()){
		if(is_array($data) && sizeof($data) > 0 && $id > 0){
			$management_position = isset($data['management_position']) && is_array($data['management_position']) && sizeof($data['management_position']) > 0 ? $data['management_position'] : ''; 
			$management_name = isset($data['management_name']) && is_array($data['management_name']) && sizeof($data['management_name']) > 0 ? $data['management_name'] : ''; 
			$completion_block = isset($data['completion_block']) && is_array($data['completion_block']) && sizeof($data['completion_block']) > 0 ? $data['completion_block'] : ''; 
			$completion_commencement = isset($data['completion_commencement']) && is_array($data['completion_commencement']) && sizeof($data['completion_commencement']) > 0 ? $data['completion_commencement'] : ''; 
			$completion_date = isset($data['completion_date']) && is_array($data['completion_date']) && sizeof($data['completion_date']) > 0 ? $data['completion_date'] : ''; 
			$completion_latest_eot = isset($data['completion_latest_eot']) && is_array($data['completion_latest_eot']) && sizeof($data['completion_latest_eot']) > 0 ? $data['completion_latest_eot'] : ''; 
			$completion_level_quantity = isset($data['completion_level_quantity']) && is_array($data['completion_level_quantity']) && sizeof($data['completion_level_quantity']) > 0 ? $data['completion_level_quantity'] : ''; 
			$completion_unit_quantity = isset($data['completion_unit_quantity']) && is_array($data['completion_unit_quantity']) && sizeof($data['completion_unit_quantity']) > 0 ? $data['completion_unit_quantity'] : ''; 
			$planed_block = isset($data['planed_block']) && is_array($data['planed_block']) && sizeof($data['planed_block']) > 0 ? $data['planed_block'] : ''; 
			$planned_fcf_submission = isset($data['planned_fcf_submission']) && is_array($data['planned_fcf_submission']) && sizeof($data['planned_fcf_submission']) > 0 ? $data['planned_fcf_submission'] : ''; 
			$planned_production_order = isset($data['planned_production_order']) && is_array($data['planned_production_order']) && sizeof($data['planned_production_order']) > 0 ? $data['planned_production_order'] : ''; 
			$planned_team_issued = isset($data['planned_team_issued']) && is_array($data['planned_team_issued']) && sizeof($data['planned_team_issued']) > 0 ? $data['planned_team_issued'] : ''; 
			$planned_installation_start = isset($data['planned_installation_start']) && is_array($data['planned_installation_start']) && sizeof($data['planned_installation_start']) > 0 ? $data['planned_installation_start'] : ''; 
			$planned_completion_date = isset($data['planned_completion_date']) && is_array($data['planned_completion_date']) && sizeof($data['planned_completion_date']) > 0 ? $data['planned_completion_date'] : ''; 
			$planned_dlp_end = isset($data['planned_dlp_end']) && is_array($data['planned_dlp_end']) && sizeof($data['planned_dlp_end']) > 0 ? $data['planned_dlp_end'] : ''; 
			
			$project_setup = array(
				'project_name' => isset($data['project_name']) && $data['project_name'] != '' ? $data['project_name'] : '',
				'project_code' => isset($data['project_code']) && $data['project_code'] != '' ? $data['project_code'] : '',
				'dev_name' => isset($data['dev_name']) && $data['dev_name'] != '' ? $data['dev_name'] : '',
				'dev_contact' => isset($data['dev_contact']) && $data['dev_contact'] != '' ? $data['dev_contact'] : '',
				'pro_company_name' => isset($data['pro_company_name']) && $data['pro_company_name'] != '' ? $data['pro_company_name'] : '',
				'pro_company_contact' => isset($data['pro_company_contact']) && $data['pro_company_contact'] != '' ? $data['pro_company_contact'] : '',
				'main_company_name' => isset($data['main_company_name']) && $data['main_company_name'] != '' ? $data['main_company_name'] : '',
				'main_company_contact' => isset($data['main_company_contact']) && $data['main_company_contact'] != '' ? $data['main_company_contact'] : '',
				'architect_name' => isset($data['architect_name']) && $data['architect_name'] != '' ? $data['architect_name'] : '',
				'architect_contact' => isset($data['architect_contact']) && $data['architect_contact'] != '' ? $data['architect_contact'] : '',
				'designer_name' => isset($data['designer_name']) && $data['designer_name'] != '' ? $data['designer_name'] : '',
				'designer_contact' => isset($data['designer_contact']) && $data['designer_contact'] != '' ? $data['designer_contact'] : '',
				'sales_team' => isset($data['sales_team']) && $data['sales_team'] != '' ? $data['sales_team'] : '',
				'project_unit' => isset($data['project_unit']) && $data['project_unit'] != '' ? $data['project_unit'] : '',
				'loa_date' => isset($data['loa_date']) && $data['loa_date'] != '' ? date('Y-m-d', strtotime($data['loa_date'])) : '',
				'dlp_date' => isset($data['dlp_date']) && $data['dlp_date'] != '' ? date('Y-m-d', strtotime($data['dlp_date']))  : '',
				'lad_amount' => isset($data['lad_amount']) && $data['lad_amount'] != '' ? $data['lad_amount'] : '',
				'target_completion_date' => isset($data['target_completion_date']) && $data['target_completion_date'] != '' ? date('Y-m-d', strtotime($data['target_completion_date'])) : '',
				'work_inprogress' => isset($data['work_inprogress']) && $data['work_inprogress'] != '' ? $data['work_inprogress'] : '',
				'remarks' => isset($data['remarks']) && $data['remarks'] != '' ? $data['remarks'] : '',
				'create_date' => isset($data['create_date']) && $data['create_date'] != '' ? date('Y-m-d', strtotime($data['create_date'])) : '',
				'create_by' => $this->session->userdata('email'),
				'update_date' => date('Y-m-d H:i:s'),
				'update_by' => $this->session->userdata('email'),
			);
			
			$this->db->where('id', $id);
			$this->db->update('project_setup', $project_setup);
			
			#delete current data from database
			$query_management = $this->db->query("SELECT * FROM `project_site_management` WHERE `project_id` = " .$this->db->escape($id));
			if($query_management->num_rows() > 0){
				$this->db->query("DELETE FROM `project_site_management` WHERE `project_id` = " .$this->db->escape($id));
			}
			
			foreach($management_position as $key_position => $val_position){
				$project_site_management = array(
					'project_id' => $id,
					'management_position' => isset($val_position) &&  $val_position != '' ? $val_position : '',
					'management_id' => isset($management_name[$key_position]) &&  $management_name[$key_position] != '' ? $management_name[$key_position] : '',
					'create_date' => date('Y-m-d H:i:s'),
					'create_by' => $this->session->userdata('email'),
					'update_date' => date('Y-m-d H:i:s'),
					'update_by' => $this->session->userdata('email'),
				);
				
				$this->db->insert('project_site_management', $project_site_management);
			}
			
			#delete current data from database
			$query_management = $this->db->query("SELECT * FROM `project_completion` WHERE `project_id` = " .$this->db->escape($id));
			if($query_management->num_rows() > 0){
				$this->db->query("DELETE FROM `project_completion` WHERE `project_id` = " .$this->db->escape($id));
			}
			
			foreach($completion_block as $key_completion => $val_completion){
				$project_completion = array(
					'project_id' => $id,
					'block' => isset($val_completion) &&  $val_completion != '' ? $val_completion : '',
					'completion_commencement' => isset($completion_commencement[$key_completion]) &&  $completion_commencement[$key_completion] != '' ? date('Y-m-d', strtotime($completion_commencement[$key_completion])) : '',
					'completion_date' => isset($completion_date[$key_completion]) &&  $completion_date[$key_completion] != '' ? date('Y-m-d', strtotime($completion_date[$key_completion])) : '',
					'completion_latest_eot' => isset($completion_latest_eot[$key_completion]) &&  $completion_latest_eot[$key_completion] != '' ? date('Y-m-d', strtotime($completion_latest_eot[$key_completion]))  : '',
					'completion_level_quantity' => isset($completion_level_quantity[$key_completion]) &&  $completion_level_quantity[$key_completion] != '' ? $completion_level_quantity[$key_completion] : '',
					'completion_unit_quantity' => isset($completion_unit_quantity[$key_completion]) &&  $completion_unit_quantity[$key_completion] != '' ? $completion_unit_quantity[$key_completion] : '',
					'create_date' => date('Y-m-d H:i:s'),
					'create_by' => $this->session->userdata('email'),
					'update_date' => date('Y-m-d H:i:s'),
					'update_by' => $this->session->userdata('email'),
				);
				
				$this->db->insert('project_completion', $project_completion);
			}
			
			#delete current data from database
			$query_management = $this->db->query("SELECT * FROM `project_planned` WHERE `project_id` = " .$this->db->escape($id));
			if($query_management->num_rows() > 0){
				$this->db->query("DELETE FROM `project_planned` WHERE `project_id` = " .$this->db->escape($id));
			}
			
			foreach($planed_block as $key_planned => $val_planned){
				$project_planned = array(
					'project_id' => $id,
					'block' => isset($val_planned) &&  $val_planned != '' ? $val_planned : '',
					'planned_fcf_submission' => isset($planned_fcf_submission[$key_planned]) &&  $planned_fcf_submission[$key_planned] != '' ? date('Y-m-d', strtotime($planned_fcf_submission[$key_planned])) : '',
					'planned_production_order' => isset($planned_production_order[$key_planned]) &&  $planned_production_order[$key_planned] != '' ? date('Y-m-d', strtotime($planned_production_order[$key_planned])) : '',
					'planned_team_issued' => isset($planned_team_issued[$key_planned]) &&  $planned_team_issued[$key_planned] != '' ? date('Y-m-d', strtotime($planned_team_issued[$key_planned])) : '',
					'planned_installation_start' => isset($planned_installation_start[$key_planned]) &&  $planned_installation_start[$key_planned] != '' ? date('Y-m-d', strtotime($planned_installation_start[$key_planned])) : '',
					'planned_completion_date' => isset($planned_completion_date[$key_planned]) &&  $planned_completion_date[$key_planned] != '' ? date('Y-m-d', strtotime($planned_completion_date[$key_planned])) : '',
					'planned_dlp_end' => isset($planned_dlp_end[$key_planned]) &&  $planned_dlp_end[$key_planned] != '' ? date('Y-m-d', strtotime($planned_dlp_end[$key_planned])) : '',
					'create_date' => date('Y-m-d H:i:s'),
					'create_by' => $this->session->userdata('email'),
					'update_date' => date('Y-m-d H:i:s'),
					'update_by' => $this->session->userdata('email'),
				);
				
				$this->db->insert('project_planned', $project_planned);
			}
			
			set_msg('Successfull', 'success');
			return true;
		}
	}
	
	#project listing start
	private function project_validate_encrypted_data($encrypted_data = ""){
		$where = "";
		$search_data = array();
		if($encrypted_data == ""){
			$encrypted_data = base64url_encode(serialize(array()));
			redirect(base_url().'setup/project_setup/'.$encrypted_data);
		}
		else{
			$search_data = unserialize(base64url_decode($encrypted_data));
			if(is_array($search_data)){
				$where = $this->project_search($search_data);
			}
			else{
				$encrypted_data = base64url_encode(serialize(array()));
				redirect(base_url().'setup/project_setup/'.$encrypted_data);
			}
		}
		
		return $where;
	}
	
	private function project_search($search_data = array()){
		$where = "";
		$sort = "";
		
		if(is_array($search_data) && sizeof($search_data) > 0){
			#Search
			if(isset($search_data['search']) && $search_data['search'] != ""){
				$where .= ($where == "" ? " WHERE " : " AND ") . " (`username` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%" ).
																	" OR `email` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `contact` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `contact` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `contact` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").")";
			}
			
			#Sorting
			if(isset($search_data['sort-column']) && trim($search_data['sort-column']) != "" && isset($search_data['sort-type']) && trim($search_data['sort-type']) != ""){
				$sort .= ($sort == "" ? " ORDER BY " : ',') . $this->db->protect_identifiers($search_data['sort-column']) . " " . strtoupper($search_data['sort-type']);
			}
		}
		
		return array('where' => $where, 'sort' => $sort);
	}
	
	function project_listing($encrypted_data = ""){
		#Get The Condition
		$condition = $this->project_validate_encrypted_data($encrypted_data);
		
		#Get WHERE
		$where = $condition['where'];
		
		#Get SORT
		$order_by = $condition['sort'] == "" ? " ORDER BY `id` DESC " : $condition['sort'];
		
		$this->page["base_url"] = base_url() . "setup/project_setup/".$this->uri->segment(3);
		$this->page["per_page"] = PER_PAGE;
		$this->page["uri_segment"] = 4;
		
		$start_form  = $this->uri->segment(4);
		$limit = " LIMIT " . $this->page["per_page"];
		if(is_numeric($start_form) && $start_form > 1){
			$limit = " LIMIT " . $start_form . ", " . $this->page["per_page"];
		}
		
		$sql_count = "SELECT COUNT(`id`) AS total FROM `project_setup`" . $where;
		$this->page["total_rows"] = $this->db->query($sql_count)->row()->total;
		$this->pagination->initialize($this->page);

		$data['query_project'] = $this->db->query("SELECT * FROM `project_setup`  " . $where . " " . $order_by . " " . $limit);
		$data['total_row'] = $this->page["total_rows"];
		$data['starting_row'] = $data['query_project']->num_rows() > 0 ? $start_form + 1 : 0;
		$data['stoping_row'] = $start_form + $data['query_project']->num_rows();
		$data['search_data'] = unserialize(base64url_decode($encrypted_data));
		$data['encrypted_data'] = $encrypted_data;
		return $data;
	}
	#project listing end
	
	public function get_detail_project($id = ''){
		$data = array();
		
		if($id > 0){
			
			$sql_setup = "SELECT * FROM `project_setup` WHERE `id` = " .$this->db->escape($id). " LIMIT 1";
			$data['project_setup'] = $this->db->query($sql_setup);
			
			$sql_completion = "SELECT * FROM `project_completion` WHERE `project_id` = " .$this->db->escape($id);
			$data['completion'] = $this->db->query($sql_completion);
			
			$sql_planned = "SELECT * FROM `project_planned` WHERE `project_id` = " .$this->db->escape($id);
			$data['planned'] = $this->db->query($sql_planned);
			
			$sql_management = "SELECT * FROM `project_site_management` WHERE `project_id` = " .$this->db->escape($id);
			$data['site_management'] = $this->db->query($sql_management);
			
			if($data['site_management']->num_rows() > 0){
				$management_id = $data['site_management']->row()->management_id;
				
				$team_query  = $this->db->query("SELECT * FROM `team_setting` WHERE `team_leader` = " .$this->db->escape($management_id). " LIMIT 1");
				if($team_query->num_rows() > 0){
					$team_id = $team_query->row()->id;
					
					$team_member = $this->db->query("SELECT `a`.*,`b`.`username` 
														FROM `team_member` AS `a` 
														INNER JOIN `user` AS `b` ON `a`.`team_member` = `b`.`id` 
														WHERE `a`.`team_id` = " .$this->db->escape($team_id));
					if($team_member->num_rows() > 0){
						foreach($team_member->result() as $row_member){
							$member[] = $row_member->username;
						}
						
						$data['site_management']->user = implode(', ', $member);
					}
				}
			}
		}
		
		return $data;
	}
}