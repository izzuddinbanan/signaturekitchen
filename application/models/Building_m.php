<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Building_m extends CI_Model 
{
	public $page = array();
	function __construct() 
	{
		parent::__construct();
	}
	
	#building_graph listing start
	private function graph_validate_encrypted_data($encrypted_data = ""){
		$where = "";
		$search_data = array();
		if($encrypted_data == ""){
			$encrypted_data = base64url_encode(serialize(array()));
			redirect(base_url().'building/list_graph/'.$encrypted_data);
		}
		else{
			$search_data = unserialize(base64url_decode($encrypted_data));
			if(is_array($search_data)){
				$where = $this->graph_search($search_data);
			}
			else{
				$encrypted_data = base64url_encode(serialize(array()));
				redirect(base_url().'building/list_graph/'.$encrypted_data);
			}
		}
		
		return $where;
	}
	
	private function graph_search($search_data = array()){
		$where = "";
		$sort = "";
		
		if(is_array($search_data) && sizeof($search_data) > 0){
			#Search
			if(isset($search_data['search']) && $search_data['search'] != ""){
				$where .= ($where == "" ? " WHERE " : " AND ") . " (`project_name` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%" ).
																	" OR `project_code` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `block_name` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `total_assign_unit` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `total_branch` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").
																	" OR `graph_revision` LIKE " . $this->db->escape("%" . $this->db->escape_like_str($search_data['search']) . "%").")";
			}
			
			#Sorting
			if(isset($search_data['sort-column']) && trim($search_data['sort-column']) != "" && isset($search_data['sort-type']) && trim($search_data['sort-type']) != ""){
				$sort .= ($sort == "" ? " ORDER BY " : ',') . $this->db->protect_identifiers($search_data['sort-column']) . " " . strtoupper($search_data['sort-type']);
			}
		}
		
		return array('where' => $where, 'sort' => $sort);
	}
	
	function graph_listing($encrypted_data = ""){
		#Get The Condition
		$condition = $this->graph_validate_encrypted_data($encrypted_data);
		
		#Get WHERE
		$where = $condition['where'];
		// $where .= ($where == "" ? " WHERE " : " AND ") . " `company_id` = " . $this->db->escape(1) . " AND `request_status` = 'pending' ";
		
		#Get SORT
		$order_by = $condition['sort'] == "" ? " ORDER BY `id` DESC " : $condition['sort'];
		
		$this->page["base_url"] = base_url() . "building/list_graph/".$this->uri->segment(3);
		$this->page["per_page"] = PER_PAGE;
		$this->page["uri_segment"] = 4;
		
		$start_form  = $this->uri->segment(4);
		$limit = " LIMIT " . $this->page["per_page"];
		if(is_numeric($start_form) && $start_form > 1){
			$limit = " LIMIT " . $start_form . ", " . $this->page["per_page"];
		}
		
		$sql_count = "SELECT COUNT(`id`) AS total FROM `building_graph`" . $where;
		$this->page["total_rows"] = $this->db->query($sql_count)->row()->total;
		$this->pagination->initialize($this->page);

		$data['query_graph'] = $this->db->query("SELECT * FROM `building_graph` " . $where . " " . $order_by . " " . $limit);
		$data['total_row'] = $this->page["total_rows"];
		$data['starting_row'] = $data['query_graph']->num_rows() > 0 ? $start_form + 1 : 0;
		$data['stoping_row'] = $start_form + $data['query_graph']->num_rows();
		$data['search_data'] = unserialize(base64url_decode($encrypted_data));
		$data['encrypted_data'] = $encrypted_data;
		return $data;
	}
	#building_graph listing end
	
	public function add_graph($project_id='', $completion_id='', $data=array()){
		if(is_array($data) && sizeof($data) > 0){
			$level = isset($data['level']) && is_array($data['level']) && sizeof($data['level']) > 0 ? $data['level'] : '';
			$unit = isset($data['unit']) && is_array($data['unit']) && sizeof($data['unit']) > 0 ? $data['unit'] : '';
			$type = isset($data['type']) && is_array($data['type']) && sizeof($data['type']) > 0 ? $data['type'] : '';
			$batch = isset($data['batch']) && is_array($data['batch']) && sizeof($data['batch']) > 0 ? $data['batch'] : '';

			$add_graph = array(
				'project_id' => $project_id,
				'project_completion_id' => $completion_id,
				'project_name' => isset($data['project_name']) && $data['project_name'] != '' ? $data['project_name'] : '',
				'project_code' => isset($data['project_code']) && $data['project_code'] != '' ? $data['project_code'] : '',
				'block_name' => isset($data['block_name']) && $data['block_name'] != '' ? $data['block_name'] : '',
				'total_assign_unit' => isset($data['total_assign_unit']) && $data['total_assign_unit'] != '' ? $data['total_assign_unit'] : '',
				'total_branch' => isset($data['total_branch']) && $data['total_branch'] != '' ? $data['total_branch'] : '',
				'graph_revision' => isset($data['graph_revision']) && $data['graph_revision'] != '' ? $data['graph_revision'] : '',
				'create_by' => $this->session->userdata('email'),
				'create_date' => date('Y-m-d H:i:s'),
				'update_by' => $this->session->userdata('email'),
				'update_date' => date('Y-m-d H:i:s'),
			);
			
			$this->db->insert('building_graph', $add_graph);
			$graph_id = $this->db->insert_id();
			
			$graph_setting = array(
				'building_graph_id' => $graph_id,
				'project_id' => $project_id,
				'project_completion_id' => $completion_id,
				'create_by' => $this->session->userdata('email'),
				'create_date' => date('Y-m-d H:i:s'),
				'update_by' => $this->session->userdata('email'),
				'update_date' => date('Y-m-d H:i:s'),
			);
			
			foreach($level as $key => $val)
			{
				foreach($unit[$val] as $keys => $vals)
				{
					$graph_setting['level'] = isset($val) && $val != '' ? $val : '';
					$graph_setting['unit'] = isset($vals) && $vals != '' ? $vals : '';
					$graph_setting['type'] = isset($type[$val][$keys]) && $type[$val][$keys] != '' ? $type[$val][$keys] : '';
					$graph_setting['batch'] = isset($batch[$val][$keys]) && $batch[$val][$keys] != '' ? $batch[$val][$keys] : '';
					
					$this->db->insert('building_graph_setting', $graph_setting);
					
				} 
			} 
			
			set_msg('Successfull', 'success');
			return true;
		}
	}
	
	public function graph_detail($id=''){
		$sql_graph = "SELECT * FROM `building_graph` WHERE `id` = " .$this->db->escape($id);
		$data['building'] = $this->db->query($sql_graph);
		
		$sql_graph_setting = "SELECT * FROM `building_graph_setting` WHERE `project_id` = " .$this->db->escape($id);
		$data['building_setting'] = $this->db->query($sql_graph_setting);
		
		return $data;
	}
	
	public function update_graph($project_id="", $completion_id="", $building_id="", $data=array()){
		if(is_array($data) && sizeof($data) > 0){
			$level = isset($data['level']) && is_array($data['level']) && sizeof($data['level']) > 0 ? $data['level'] : '';
			$unit = isset($data['unit']) && is_array($data['unit']) && sizeof($data['unit']) > 0 ? $data['unit'] : '';
			$type = isset($data['type']) && is_array($data['type']) && sizeof($data['type']) > 0 ? $data['type'] : '';
			$batch = isset($data['batch']) && is_array($data['batch']) && sizeof($data['batch']) > 0 ? $data['batch'] : '';
			
			$graph = array(
				'project_id' => $project_id,
				'project_completion_id' => $completion_id,
				'project_name' => isset($data['project_name']) && $data['project_name'] != '' ? $data['project_name'] : '',
				'project_code' => isset($data['project_code']) && $data['project_code'] != '' ? $data['project_code'] : '',
				'block_name' => isset($data['block_name']) && $data['block_name'] != '' ? $data['block_name'] : '',
				'total_assign_unit' => isset($data['total_assign_unit']) && $data['total_assign_unit'] != '' ? $data['total_assign_unit'] : '',
				'total_branch' => isset($data['total_branch']) && $data['total_branch'] != '' ? $data['total_branch'] : '',
				'graph_revision' => isset($data['graph_revision']) && $data['graph_revision'] != '' ? $data['graph_revision'] : '',
				'update_by' => $this->session->userdata('email'),
				'update_date' => date('Y-m-d H:i:s'),
			);
			
			$this->db->where('id', $building_id);
			$this->db->update('building_graph', $graph);
			
			$sql_check = "SELECT * FROM `building_graph_setting` WHERE `building_graph_id` = " .$this->db->escape($building_id);
			$query_check = $this->db->query($sql_check);
			
			if($query_check->num_rows() > s0){
				$this->db->query("DELETE FROM `building_graph_setting` WHERE `building_graph_id` = " .$this->db->escape($building_id));
			}
			
			$graph_setting = array(
				'building_graph_id' => $building_id,
				'project_id' => $project_id,
				'project_completion_id' => $completion_id,
				'create_by' => $this->session->userdata('email'),
				'create_date' => date('Y-m-d H:i:s'),
				'update_by' => $this->session->userdata('email'),
				'update_date' => date('Y-m-d H:i:s'),
			);
			
			foreach($level as $key => $val)
			{
				foreach($unit[$val] as $keys => $vals)
				{
					$graph_setting['level'] = isset($val) && $val != '' ? $val : '';
					$graph_setting['unit'] = isset($vals) && $vals != '' ? $vals : '';
					$graph_setting['type'] = isset($type[$val][$keys]) && $type[$val][$keys] != '' ? $type[$val][$keys] : '';
					$graph_setting['batch'] = isset($batch[$val][$keys]) && $batch[$val][$keys] != '' ? $batch[$val][$keys] : '';
					
					$this->db->insert('building_graph_setting', $graph_setting);
					
				} 
			} 
			
			set_msg('Successfull', 'success');
			return true;
		}
	}
	
	public function get_project_setup($project_id='', $completion_id=''){
		if($project_id > 0 && $completion_id > 0){
			
			$sql_project = "SELECT `a`.`project_name`, `a`.`project_code`, `b`.`*` 
								FROM `project_setup` AS `a` 
								LEFT JOIN `project_completion` AS `b` 
								ON `a`.`id` = `b`.`project_id`
								WHERE `b`.`project_id` = " .$this->db->escape($project_id) ." AND `b`.`id` = " .$this->db->escape($completion_id). " LIMIT 1";
								
			return $this->db->query($sql_project)->row();
		}
	}
	
	public function get_building_graph($return_type = "query", $condition = array()){
		$where = "";
		
		#If condition is array, loop to extract where condition into string
		if(is_array($condition)){
			if(sizeof($condition) > 0){
				foreach($condition as $column_name => $column_value){
					$where .= ($where == "" ? " WHERE " : " AND ") . $this->db->protect_identifiers($column_name) . " = " . $this->db->escape($column_value);
				}
			}
		}
		else{
			#Condition in string contains where condition
			$where = $condition;
		}
		
		$query = $this->db->query("SELECT * FROM `building_graph_setting` $where");
		if($return_type == "row"){
			if($query->num_rows() > 0){
				return $query->row();
			}
			
			return new stdClass();
		}
		
		return $query;
	}
}