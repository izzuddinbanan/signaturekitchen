<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_m extends CI_Model 
{
	function __construct() 
	{
		parent::__construct();
	}
	
	public function login_verify($data=array()){
		if(is_array($data) && sizeof($data) > 0){
			$email = trim($data['email']);
			$password = trim($data['password']);
			
			if(isset($email) && $email == ''){
				return false;
			}
			// else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
				// return false;
			// }
			
			if(isset($password) && $password == ''){
				return false;
			}
			
			$sql = "SELECT *, DATE_FORMAT(last_login, '%d %b %Y %r') AS last_logged FROM `user` WHERE `email` = " .$this->db->escape($email). " AND `password` = " .$this->db->escape(md5($password)). " LIMIT 1";
			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				$row = $query->row();
				
				if($row->active == '1'){ //check if user active
					$session = array(
						'id' => isset($row->id) && $row->id != "" ? $row->id : "",
						'username' => isset($row->username) && $row->username != "" ? $row->username : "",
						'email' => isset($row->email) && $row->email != "" ? $row->email : "",
						'team' => isset($row->team) && $row->team != "" ? $row->team : "",
						'role' => isset($row->role) && $row->role != "" ? $row->role : "",
						'logged_in' => TRUE, 
						'last_logged' => $row->last_logged,
					);
					
					$this->session->set_userdata($session);
					
					$query_update = $this->db->query("UPDATE `user` SET `last_login` = NOW() WHERE `id` = " .$this->db->escape($row->id));
					
					return true;
				}
				else{
					return false;
				}
			}
			else{
				return false;
			}
			
		} 
	}
	
	public function user_validate(){
		$sql = "SELECT * FROM `user` WHERE `active` = '1' AND `email` = " .$this->db->escape($this->session->userdata('email')). " AND `id` = " .$this->db->escape($this->session->userdata('id')). " LIMIT 1"; 
		$query = $this->db->query($sql);
		return $query->row();
	}
}